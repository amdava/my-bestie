import "./App.css";
import { SignupPage } from "./container/auth/signup/signup";
import { LoginPage } from "./container/auth/login/login";
import { ForgetPassword } from "./container/auth/login/forgetpass";
import { LandingPage } from "./container/Home/LandingPage";
import { HomePage } from "./container/HomePage/HomePage";
import { ProfilePage } from "./container/profile/profilePage";
import { NewPost } from "./container/profile/post/post";
import { Settings } from "./container/profile/settings/settings";
import { EditProfile } from "./container/profile/settings/components/EditProfile/EditProfile";
import { BankPage } from "./container/profile/settings/components/BankDetails/BankDetails";
import { ReferallsPage } from "./container/profile/settings/components/Referalls/Referalls";
import { BankAddStarPage } from "./container/profile/settings/components/BankAddStars/BankAddStars";
import { SendAddStarPage } from "./container/profile/settings/components/SendAddStars/SendAddStars";
import { Notifications } from "./container/notifications/notification";
import { Creatorpage } from "./container/Explore/Creators/creatorpage";
import { CreatorsField } from "./container/Explore/CreatorsField/CreatorsField";
import { CreatorProfile  } from './container/Explore/CreatorProfile/CreatorProfile'
// import { CreatorPost } from "./container/Explore/CreatorProfile/CreatorPosts";
// import { CreatorSubscribers } from "./container/Explore/CreatorProfile/CreatorSubscribers";
// import { CreatorAbout } from "./container/Explore/CreatorProfile/CreatorAbout";
// import { Messages } from "./component/Messages/messages";
import { Chatopened } from "./container/Messages/components/chatopened";
import { Dashboard } from "./container/Dashboard/dashboard";
import { Admindashboard } from "./container/Dashboard/admindashboard";
import { Page404 } from "./component/Errors/Page404";

import { AuthProvider } from "./contexts/AuthContext";
import { PrivateRoute } from "./privateRoute/privateroute";
import { GlobalRoute } from "./privateRoute/globalroute";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "./redux/store";
function App() {
  return (
    <Provider store={store}>
      <AuthProvider>
        <Router>
          <div className="App">
            <Switch>
              <GlobalRoute exact path="/" component={LandingPage} />
              <GlobalRoute exact path="/signup" component={SignupPage} />
              <GlobalRoute exact path="/login" component={LoginPage} />
              <GlobalRoute
                exact
                path="/login/reset-password"
                component={ForgetPassword}
              />
              <PrivateRoute exact path="/homepage" component={HomePage} />
              <PrivateRoute exact path="/profile" component={ProfilePage} />
              <PrivateRoute exact path="/newpost" component={NewPost} />
              <PrivateRoute exact path="/settings" component={Settings} />
              <PrivateRoute
                exact
                path="/settings/edit-profile"
                component={EditProfile}
              />
              <PrivateRoute
                exact
                path="/settings/bank-details"
                component={BankPage}
              />
              <PrivateRoute
                exact
                path="/settings/referalls"
                component={ReferallsPage}
              />
              <PrivateRoute
                exact
                path="/settings/add-stars-to-balance"
                component={BankAddStarPage}
              />
              <PrivateRoute
                exact
                path="/settings/sendstars"
                component={SendAddStarPage}
              />
              <PrivateRoute
                exact
                path="/notifications"
                component={Notifications}
              />
              <PrivateRoute exact path="/explore" component={Creatorpage} />
              <PrivateRoute
                exact
                path="/explore/creators-profile"
                component={CreatorsField}
              />
              <PrivateRoute
                exact
                path="/explore/creators-profile-details"
                component={CreatorProfile}
              />
              {/* <PrivateRoute
                exact
                path="/explore/creators-posts"
                component={CreatorPost}
              />
              <PrivateRoute
                exact
                path="/explore/creators-subscribers"
                component={CreatorSubscribers}
              />
              <PrivateRoute
                exact
                path="/explore/creators-about"
                component={CreatorAbout}
              /> */}
              {/* <PrivateRoute exact path="/messages" component={Messages} /> */}
              <PrivateRoute exact path="/messages" component={Chatopened} />
              <PrivateRoute exact path="/dashboard" component={Dashboard} />
              <PrivateRoute
                exact
                path="/admin-dashboard"
                component={Admindashboard}
              />
              <Route component={Page404} />
            </Switch>
          </div>
        </Router>
      </AuthProvider>
    </Provider>
  );
}

export default App;
