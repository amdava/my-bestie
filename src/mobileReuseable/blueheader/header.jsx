import classes from "./header.module.scss";
import React from "react";
import { Link } from "react-router-dom";
export const Header = ({ heading, imgLeft, imgRight ,backTo , forwardTo , heading2}) => {
  return (
    <div className={classes.mobileHeader}>
      <div className={classes.header}>
        <Link to={backTo}>
          <img src={imgLeft} alt="" className={classes.lefticon}/>
        </Link>
        <Link to={forwardTo}>
          <img src={imgRight} alt="" className={classes.righticon}/>
        </Link>
      </div>
      <div className={classes.Headerheading}>{heading}</div>
      <div className={classes.Headerheading2}>{heading2}</div>
    </div>
  );
};
