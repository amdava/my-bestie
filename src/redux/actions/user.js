import {
	USER_DATA_REQUEST,
	USER_DATA_SUCCESS,
	USER_DATA_FAIL,
	GET_USER_DATA_REQUEST,
	GET_USER_DATA_SUCCESS,
	GET_USER_DATA_FAIL,
} from "../Types";

import { store } from "../../firebase";

export const getuserdata = (uid) => (dispatch) => {
	console.log(uid);
	try {
		dispatch({ type: USER_DATA_REQUEST });

		store
			.collection("users")
			.doc(uid)
			.onSnapshot((doc) => {
				if (doc.exists) {
					dispatch({ type: USER_DATA_SUCCESS, payload: doc.data() });
				} else {
					console.log("not exit");
				}
			});
	} catch (error) {
		dispatch({ type: USER_DATA_FAIL, payload: error });
	}
};

export const getSelectedUser = (uid) => (dispatch) => {
	console.log(uid);
	try {
		dispatch({ type: GET_USER_DATA_REQUEST });
		var userData = {};
		store
			.collection("users")
			.doc(uid)
			.onSnapshot((doc) => {
				if (doc.exists) {
					dispatch({ type: GET_USER_DATA_SUCCESS, payload: doc.data() });
				} else {
					console.log("not exit");
				}
				// userData = snapshot.data();
			});
	} catch (error) {
		dispatch({ type: GET_USER_DATA_FAIL, payload: error });
	}
};
