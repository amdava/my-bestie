import { combineReducers } from "redux";
import { UserReducer , UsersReducer} from "./reducer/userReducer";
import { StatusReducer } from "./reducer/statusReducer";
// import { ProductReducer } from "./reducer/productReducer";

export const rootReducers = combineReducers({
  UserReducer,
  StatusReducer,
  UsersReducer
  // ProductReducer
});
