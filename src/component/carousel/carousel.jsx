import React from "react";
// import { Modal, Carousel } from "antd";
// import "./style.css";
import { AutoRotatingCarousel, Slide } from "material-auto-rotating-carousel";
import { red, blue, green } from "@material-ui/core/colors";

export const CarouselFn = ({ isWatch, toggle, data }) => {
  return (
    <div style={{ position: "relative", width: "100%", height: 500 }}>
      {/* <Button onClick={() => setState({ open: true })}>Open carousel</Button> */}
      <AutoRotatingCarousel
        // label="Get started"
        open={isWatch}
        onClose={toggle}
        onStart={toggle} 
        style={{ position: "absolute" }}
      >
        {data.map((item, index) => {
          return (
            // <div key={index} className="bg-gray-500">
            //   <img src={item.url} alt="" className="h-3/5 w-full sm:h-96" />
            // </div>
            <Slide
              key={index}
              media={
                <img src={item.url} alt="" className="h-3/5 w-full sm:h-96 object-cover" />
              }
              mediaBackgroundStyle={{ backgroundColor: "#2bc2d3" }}
              style={{ backgroundColor: "#2bc2d3" }}
              title="This is a very cool feature"
              subtitle="Just using this will blow your mind."
            ></Slide>
          );
        })}
      </AutoRotatingCarousel>
    </div>
    //     <Modal
    //       className="w-full min-h-screen .ant-modal-body .ant-modal-content"
    //       title="User Name"
    //       centered
    //       visible={isWatch}
    //       onOk={toggle}
    //       onCancel={toggle}
    //       title={false}
    //       footer={false}
    //       width={800}

    // >
    //       <Carousel
    //         dotPosition="bottom"
    //         autoplay={true}
    //         speed={100}
    //         className=".ant-carousel .slick-dots li .ant-carousel .slick-dots li.slick-active .ant-carousel .slick-dots-bottom"
    //       >
    //         {data.map((item, index) => {
    //           return (
    //             <div key={index} className="pt-36 sm:pt-28 sm:px-10 bg-gray-900 h-screen">
    //               <img src={item.url} alt="" className="h-3/5 w-full sm:h-96" />
    //             </div>
    //           );
    //         })}
    //       </Carousel>
    //     </Modal>
  );
};
