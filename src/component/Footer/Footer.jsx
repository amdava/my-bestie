import React from "react";
import classes from "./Footer.module.scss";
import { Link } from "react-router-dom";
export const Footer = ({ Home, Explore, NewPost, Msg, Profile }) => {
  return (
    <div className={classes.Footer}>
      <div className={classes.MobileFooter}>
        <Link to="/homepage" className={Home? classes.Active : classes.NotActive}>
          {Home ? (
            <img src="/img/mobilefooter/home1.png" alt="" />
          ) : (
            <img src="/img/mobilefooter/home0.png" alt="" />
          )}
        </Link>
        <Link to="/explore" className={Explore? classes.Active : classes.NotActive}>
          {Explore ? (
            <img src="/img/mobilefooter/Explore1.png" alt="" />
          ) : (
            <img src="/img/mobilefooter/Explore0.png" alt="" />
          )}
        </Link>
        <Link to="/profile" className={NewPost? classes.Active : classes.NotActive}>
          {NewPost ? (
            <img src="/img/mobilefooter/post0.png" alt="" />
          ) : (
            <img src="/img/mobilefooter/post0.png" alt="" />
          )}
        </Link>
        <Link to="/notifications" className={ Msg ? classes.Active : classes.NotActive}>
          {Msg ? (
            <img src="/img/mobilefooter/sms0.png" alt="" />
          ) : (
            <img src="/img/mobilefooter/sms0.png" alt="" />
          )}
        </Link>
        <Link to="/settings" className={Profile ? classes.Active : classes.NotActive}>
          {Profile ? (
            <img src="/img/mobilefooter/Profile1.png" alt="" />
          ) : (
            <img src="/img/mobilefooter/Profile0.png" alt="" />
          )}
        </Link>
      </div>
    </div>
  );
};
