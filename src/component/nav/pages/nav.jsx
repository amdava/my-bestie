import React from "react";
import { Input } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import classes from "./nav.module.css";
import { Link } from "react-router-dom";

export const Nav = () => {
  return (
    <div className={classes.Navbar}>
      <div className={classes.Navbarstart}>
        <Link to="/">
          <div className={classes.LogoHandler}></div>
        </Link>
        <Input
          className={classes.SearchBar}
          placeholder="Find a creator"
          suffix={<SearchOutlined className={classes.SearchIcon} />}
        />
        <div className={classes.Navtext}>EXPLORE</div>
      </div>
      <div className={classes.NavbarEnd}>
        <Link to="/homepage"><img src="/img/15.png" className={classes.imgStyle} alt=""/></Link>
        <Link to="/explore"><img src="/img/16.png" className={classes.imgStyle} alt=""/></Link>
        <Link to="/profile"><img src="/img/17.png" className={classes.imgStyle} alt=""/></Link>
        <Link to="/notifications"><img src="/img/18.png" className={classes.imgStyle} alt=""/></Link>
        <Link to="/settings"><img src="/img/19.png" className={classes.imgStyle} alt=""/></Link>
      </div>
    </div>
  );
};
