import React from "react";
import { Input, Button } from "antd";
import { ArrowRightOutlined, SearchOutlined } from "@ant-design/icons";
import classes from "./nav.module.css";
import { Link } from "react-router-dom";

export const Nav = () => {
  return (
    <div className={classes.Navbar}>
      <div className={classes.Navbarstart}>
        <Link to="/">
          <div className={classes.LogoHandler}></div>
        </Link>
        <Input
          className={classes.SearchBar}
          placeholder="Find a creator"
          suffix={<SearchOutlined className={classes.SearchIcon} />}
        />
        <Link to="/explore" className={classes.Navtext}>EXPLORE</Link>
      </div>
      <div className={classes.NavbarEnd}>
        <Link to="/login">
          <Button className={classes.LogIn}>LOG IN</Button>
        </Link>
        <Link to="/signup">
          <Button className={classes.GettingStarted}>
            Getting Started
            <ArrowRightOutlined />
          </Button>
        </Link>
      </div>
    </div>
  );
};
