import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/database";
import "firebase/storage";
const app = firebase.initializeApp({
  // apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  // authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  // projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  // storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  // messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  // appId: process.env.REACT_APP_FIREBASE_APP_ID,
  apiKey: "AIzaSyDEwuj2VR2btj9JykMKT9dfBABlCUjXsWc",
  authDomain: "uvuew-web.firebaseapp.com",
  projectId: "uvuew-web",
  storageBucket: "uvuew-web.appspot.com",
  messagingSenderId: "431523039280",
  appId: "1:431523039280:web:509326d30ab3dcbfb885f5",
  measurementId: "G-HYZN2R84M6"
});

export const auth = app.auth();
export const store = app.firestore();
export const db = app.database();
export const storage = app.storage();
export default app;
