import React, { createContext, useContext, useState, useEffect } from "react";
import { auth, store } from "../firebase";
import firebase from "firebase/app";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getuserdata } from "../redux/actions/user";
import { message } from "antd";
const AuthContext = createContext();

export const useAuth = () => {
	return useContext(AuthContext);
};

export const AuthProvider = ({ children }) => {
	const [currentUser, setcurrentUser] = useState();
	const dispatch = useDispatch();
	const history = useHistory();
	var provider = new firebase.auth.TwitterAuthProvider();

	const signup = (email, password) => {
		return auth.createUserWithEmailAndPassword(email, password);
	};

	function twitterSignout() {
		firebase
			.auth()
			.signOut()

			.then(
				function () {
					console.log("Signout successful!");
				},
				function (error) {
					console.log("Signout failed!");
				}
			);
	}

	const OnTwitterLogin = () => {
		console.log(provider);
		auth
			.signInWithPopup(provider)
			.then((result) => {
				console.log(result);
				// var token = result.credential.accessToken;
				// var user = result.user;
				const { screen_name, profile_image_url, description } = result.additionalUserInfo.profile;

				store
					.collection("users")
					.doc(result.user.uid)
					.get()
					.then(async (doc) => {
						if (doc.exists) {
							await dispatch(getuserdata(result.user.uid));
							localStorage.setItem("isLoggedin", 1);
							message.success("Success: You are successfully logged in.");
							//history.push("/homepage");
						} else {
							store
								.collection("users")
								.doc(result.user.uid)
								.set({
									userName: screen_name,
									email: screen_name,
									password: "NAN",
									dob: "NAN",
									createdAt: new Date(),
									uid: result.user.uid,
									IsOnline: true,
									photo: result.user.photoURL,
									state: "NAN",
									dob: "NAN",
									haveStatus: false,
									userType: "normal",
									cover: "",
									coins: 0,
								})
								.then(async (docRef) => {
									await localStorage.setItem("isLoggedin", 1);
									//history.push("/homepage");
								});
						}
					});
			})
			.catch((error) => {});
	};

	const login = (email, password) => {
		return auth.signInWithEmailAndPassword(email, password);
	};

	const logout = () => {
		localStorage.setItem("isLoggedin", 0);
		return auth.signOut();
	};

	const resetPassword = (email) => {
		return auth.sendPasswordResetEmail(email);
	};

	const value = {
		currentUser,
		signup,
		login,
		logout,
		resetPassword,
		OnTwitterLogin,
		twitterSignout,
	};

	useEffect(() => {
		const unsubsribe = auth.onAuthStateChanged((user) => {
			setcurrentUser(user);
			if (user) {
				dispatch(getuserdata(user.uid));
				console.log("authchanged called");
			} else {
			}
		});
		return unsubsribe;
	}, []);
	return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};
