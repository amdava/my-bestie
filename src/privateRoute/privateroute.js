import React from "react";
import { useAuth } from "../contexts/AuthContext";
import { Redirect, Route } from "react-router-dom";
export const PrivateRoute = ({ component: Component, ...rest }) => {
  const { currentUser } = useAuth();
  return (
    <Route
      {...rest}
      render={(props) => {
        return currentUser | JSON.parse(localStorage.getItem('isLoggedin')) === 1 ?  <Component /> : <Redirect to="/login" />;
      }}
    />
  );
};
