import React from "react";
import classes from "./style.module.scss";
import { CreatorsField } from "../CreatorsField/CreatorsField";
import { Profile } from "./profile/profile";
import { Nav } from "../../../component/nav/pages/nav";
import { Footer } from "../../../component/Footer/Footer";
export const CreatorProfile = () => {
  return (
    <>
      <Nav />
      <div className={classes.container}>
        <div className={classes.exploreHandler}>
          <CreatorsField dualscreen={true} />
        </div>
        <div className={classes.profileHandler}>
          <Profile/>
        </div>
      </div>
      <Footer Explore={true}/>
    </>
  );
};
