import React from "react";
import classes from "./style.module.scss";
import { CreatorsField } from "../CreatorsField/CreatorsField";
import { Profile } from "../../profile/profile/profile";
import { Nav } from "../../../component/nav/pages/nav";
import { Footer } from "../../../component/Footer/Footer";
export const CreatorSubscribers = () => {
  return (
    <>
      <Nav />
      <div className={classes.container}>
        <div className={classes.exploreHandler}>
          <CreatorsField dualscreen={true} />
        </div>
        <div className={classes.profileHandler}>
          <Profile creatorProfile={true} creatorSubscribers={true} ActiveSub={true} />
        </div>
      </div>
      <Footer Explore={true}/>
    </>
  );
};
