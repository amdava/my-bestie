import React from "react";
import { Card, Image, Row, Col, Avatar } from "antd";
import classes from "./post.module.css";
const { Meta } = Card;
export const Post = ({ userData }) => {
	const {
		id,
		title,
		text,
		uid,
		location,
		avatar,
		Privacy,
		createdAt,
		createdBy,
		email,
		img,
		MediaSrc,
		// Likes,
		// comments,
	} = userData;

	// console.log(creatorProfile)
	const AddLike = (value) => {
		const data = { ...userData };
		console.log("I'm loaded", userData, value);
		data.Likes += 1; 
	}; 

	const GetUserID = (id) => {
		console.log(id);
	};
	return (
		<Card className={classes.post} onClick={() => GetUserID(id)}>
			<Meta
				className={classes.posthandler}
				avatar={
					<Avatar
						src={
							avatar
								? avatar
								: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQBB2V9omR8vXGvd3IekVOr4Y4VPD9OOd_srQ&usqp=CAU"
						}
						className={classes.avatarStyle}
					/>
				}
				title={
					<div className={classes.CardHeader}>
						<span
							style={{
								display: "flex",
								flexDirection: "column",
								alignItems: "left",
							}}
						>
							<span className={classes.UserName}>{createdBy}</span>
							<span className={classes.UserAddress}>@{createdBy}</span>
						</span>
						<span className={classes.PostArrow}>
							<img src="img/settings2.png" alt="" />
						</span>
					</div>
				}
				description={
					<div className={classes.PostData}>
						<div className={classes.showtext}>
							<Row className={classes.postheading}>{title}</Row>
							<Row className={classes.postparagraph}>{text.substring(0, 50) + "..."}</Row>
						</div>
						{img === "" ? null : (
							<div className={classes.PostMedia}>
								<Image preview={false} alt="example" src={img} />
							</div>
						)}
						<Row justify="space-between" className={classes.menu}>
							<Col>
								<Row justify="space-between" style={{ width: "200px" }}>
									<Col>
										<img src="img/dollar.png" alt="" />
									</Col>
									<Col className="flex">
										<span className={classes.postText}>0</span>
										&nbsp;&nbsp;
										<span>
											<img src="img/21.png" alt="" />
										</span>
									</Col>
									<Col className="flex">
										<span className={classes.postText}>0</span>
										&nbsp;&nbsp;
										<span onClick={() => AddLike(id)}>
											<img src="img/heart.png" alt="" />
										</span>
									</Col>
								</Row>
							</Col>
						</Row>
					</div>
				}
			/>
		</Card>
	);
};
