import React from "react";
import classes from "./style.module.scss";
import { Button, Image } from "antd";
import { useHistory } from "react-router-dom";

export const Card = ({
  Creatorimg,
  Creatorname,
  Creatorstatus,
  index,
  bg,
  textcolor,
  arrow,
}) => {
  const history = useHistory();
  const OnOpenProfile = () => {
    history.push("/explore/creators-profile");
  };
  return (
    <div className={classes.card}>
      <div className={classes.AvatarHandler} style={{ zIndex: -index }}>
        <Image
          src={Creatorimg}
          alt=""
          className={classes.Avatar}
          preview={false}
        />
      </div>
      <div
        className={classes.CardInfo}
        style={{ backgroundColor: bg, color: textcolor }}
      >
        <div>{Creatorname}</div>
        <div>{Creatorstatus}</div>
        <br />
        <Button
          onClick={OnOpenProfile}
          className={classes.Button}
          style={{
            backgroundColor: bg,
            color: textcolor,
            border: `1px solid ${textcolor}`,
          }}
        >
          <span>Explore</span>
          <img src={arrow} alt="" />
        </Button>
      </div>
    </div>
  );
};
