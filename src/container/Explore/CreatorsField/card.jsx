import React, { useState } from "react";
import classes from "./style.module.scss";
import { Avatar } from "antd";
import { useHistory } from "react-router-dom";
export const Card = ({ Creatorimg, Creatorname, Creatorstatus, status }) => {
  const [EditOption, setEditOption] = useState(false);
  const history = useHistory();
  const OnClickCreatorProfile = () => {
    history.push('/explore/creators-profile-details')
  };
  return (
    <div className={classes.card} >
      <div className={EditOption ? classes.hide : classes.CardHandler}>
        <div className={status ? classes.IsOnline : classes.Offline}></div>
        <Avatar
          onClick={OnClickCreatorProfile}
          src={Creatorimg}
          alt=""
          shape="square"
          className={classes.Avatar}
        />
        <div className={classes.textHandler} onClick={OnClickCreatorProfile}>
          <div>{Creatorname}</div>
          <div>{Creatorstatus}</div>
        </div>
        <img
          src="/img/options-arrow.png"
          alt=""
          className={classes.settings}
          onClick={() => setEditOption(true)}
        />
      </div>
      <div className={EditOption ? classes.Options : classes.hide}>
        <div className={classes.textHandler}>
          <div>{Creatorname}</div>
          <div>{Creatorstatus}</div>
        </div>
        <div className={classes.comment}>
          <img src="/img/msg.png" alt="" />
        </div>
        <div className={classes.gift}>
          <img src="/img/gift.png" alt="" />
        </div>
        <img
          src="/img/close-icon.png"
          alt=""
          className={classes.settings}
          onClick={() => setEditOption(false)}
        />
      </div>
    </div>
  );
};
