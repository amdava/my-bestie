import React, { useState } from "react";
import classes from "./style.module.scss";
import { Card } from "./card";
import { Link, useHistory } from "react-router-dom";
import { Nav } from "../../../component/nav/pages/nav";
import { Footer } from "../../../component/Footer/Footer";
export const CreatorsField = ({ dualscreen }) => {
	const [creators, setcreators] = useState([
		{
			Creatorname: "Sara Dravid",
			Creatorstatus: "Developer",
			Creatorimg: "https://ak.picdn.net/offset/photos/5b02cf9517fb156e48080fed/medium/photo.jpg",
			status: true,
		},
		{
			Creatorname: "Willian M",
			Creatorstatus: "Painter",
			Creatorimg:
				"https://hips.hearstapps.com/hbz.h-cdn.co/assets/17/15/hbz-hottest-men-george-clooney-gettyimages-178273260.jpg?crop=1.0xw:1xh;center,top&resize=480:*",
			status: false,
		},
		{
			Creatorname: "Mark Wood",
			Creatorstatus: "Photographer",
			Creatorimg:
				"https://images.unsplash.com/photo-1581382575275-97901c2635b7?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8bWFufGVufDB8fDB8&ixlib=rb-1.2.1&w=1000&q=80",
			status: true,
		},
		{
			Creatorname: "Elina Doe",
			Creatorstatus: "Designer",
			Creatorimg:
				"https://images.pexels.com/photos/1181424/pexels-photo-1181424.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
			status: false,
		},
		{
			Creatorname: "Mina Meyer",
			Creatorstatus: "Developer",
			Creatorimg:
				"https://mc.webpcache.epapr.in/mcms.php?size=medium&in=https://mcmscache.epapr.in/post_images/website_154/post_18868303/thumb.jpg",
			status: true,
		},
	]);

	const history = useHistory();
	const MoveToPage = () => {
		history.push("/messages");
	};
	return (
		<>
			{dualscreen ? null : <Nav />}
			<div className={dualscreen ? classes.dualcontainer : classes.container}>
				<div className={classes.mobileHeader}>
					<div className={classes.header}>
						<Link to={dualscreen ? "/explore/creators-profile-details" : "/explore"}>
							<img src="/img/arrow-left.png" alt="" />
						</Link>
						<Link to="/explore">
							<img src="/img/search.png" alt="" />
						</Link>
					</div>
					{/* status came from the Explore page */}
					<div className={classes.Headerheading}>Designer</div>
					<div className={classes.text}>
						<div style={{ borderBottom: "3px solid white" }}>Subscribers</div>
						<div onClick={MoveToPage}>Message</div>
					</div>
				</div>
				<div className={classes.creatorsHandler}>
					{creators.map((item, index) => {
						return (
							<Card
								key={index}
								Creatorname={item.Creatorname}
								Creatorstatus={item.Creatorstatus}
								Creatorimg={item.Creatorimg}
								status={item.status}
							/>
						);
					})}
				</div>
			</div>
			{dualscreen ? null : <Footer Explore={true} />}
		</>
	);
};
