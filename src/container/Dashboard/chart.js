import React from "react";
import { Line } from "react-chartjs-2";
export const Chart = () => {
  const data = {
    labels: [
      "Feb 20",
      "Mar 20",
      "Apr 20",
      "June 20",
      "May 20",
      "Aug 20",
      "Sep 20",
      "Oct 20",
      "Nov 20",
      "Dec 20",
      "Jan 21",
      "Feb 21",
    ],
    datasets: [
      {
        label: "Statistics of the months",
        data: [5, 15, 3, 12, 18, 1, 22, 8, 12, 15, 16, 2, 13],
        pointBackgroundColor: "#2BC2D3",
        pointBorderColor: "#2BC2D3",
        pointBorderWidth: 10,
        showLine: false,
      },
      {
        label: "Statistics of the months",
        data: [5, 20, 13, 10, 10, 5, 12, 22, 1, 1, 6, 12, 23],
        pointBackgroundColor: "#51F0B0",
        pointBorderColor: "#51F0B0",
        pointBorderWidth: 10,
        showLine: false,
      },
    ],
  };

  const Options = {
    responsive: true,
    maintainAspectRatio: true,
    legend: {
      display: false,
    },
    scales: {
      yAxes: [
        {
          ticks: {
            max: 55,
            min: 0,
            stepSize: 5,
            callback: function(value , index , values){
                return '$' + value
            }
          },
        },
      ],
    },
  };
  return <Line data={data} options={Options} />;
};
