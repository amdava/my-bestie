import React, { useState } from "react";
import classes from "./style.module.scss";


export const Sidebar = ({ item , index , dashboardItem}) => {
  const [tabstatus] = useState(dashboardItem);

  // const OnMoveToPage = (index) => {
  //   settabstatus(index);
  // };
  const arrowImageWhite = (
    <img src="/img/arrow-icon-white.png" alt="" width="15px" height="23px" />
  );
  
  const arrowImageGreen = (
    <img src="/img/arrow-icon-green.png" alt="" width="15px" height="23px" />
  );
  return (
    <div
      className={
        tabstatus === index ? classes.SettingsTabActive : classes.SettingsTab
      }
    //   onClick={() => OnMoveToPage(index)}
    >
      <span>{item.itemName}</span>
      <>{tabstatus === item.itemName ? arrowImageWhite : arrowImageGreen}</>
    </div>
  );
};
