import React, { useState, useRef } from "react";
import { MapContainer, TileLayer, MapStyle } from "react-leaflet";
import "./map.css";
import "leaflet/dist/leaflet.css";
export const Leafletmap = () => {
  const [center, setcenter] = useState({ lat: 30.3753, lng: 69.3451 });
  const ZOOM_LEVEL = 9;
  const mapRef = useRef();
  return (
    <div>
      <MapContainer center={center} zoom={ZOOM_LEVEL} ref={mapRef}>
        <TileLayer
          url="https://api.maptiler.com/maps/basic/256/{z}/{x}/{y}.png?key=bzS1TRbhpm6uigxQNXiC"
          attribution='<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>'
        />
      </MapContainer>
    </div>
  );
};
