import React, { useState } from "react";
import { Nav } from "../../component/nav/pages/nav";
import { Header } from "../../mobileReuseable/blueheader/header";
import { Sidebar } from "./sidebar";
import { Footer } from "../../component/Footer/Footer";
import classes from "./style.module.scss";
import { Chart } from "./chart";
import { Leafletmap } from "./leafletmap";
import { Avatar } from "antd";
export const Dashboard = ({ isAdmin, dashboardItem }) => {
  const [adminmenuItem] = useState([
    {
      itemName: "Dashboard",
      linkTo: "",
    },
    {
      itemName: "General Settings",
      linkTo: "",
    },
    {
      itemName: "Maintenance Mode",
      linkTo: "",
    },
    {
      itemName: "Billing Information",
      linkTo: "",
    },
    {
      itemName: "Email Settings",
      linkTo: "",
    },
    {
      itemName: "Storage",
      linkTo: "",
    },
    {
      itemName: "Theme",
      linkTo: "",
    },
    {
      itemName: "Posts",
      linkTo: "",
    },
    {
      itemName: "Subscriptions",
      linkTo: "",
    },
    {
      itemName: "Transactions",
      linkTo: "",
    },
    {
      itemName: "Members",
      linkTo: "",
    },
    {
      itemName: "Languages",
      linkTo: "",
    },
    {
      itemName: "Categories",
      linkTo: "",
    },
    {
      itemName: "Reports",
      linkTo: "",
    },
    {
      itemName: "Withdrawls",
      linkTo: "",
    },
    {
      itemName: "Verification Requests",
      linkTo: "",
    },
    {
      itemName: "Pages",
      linkTo: "",
    },
    {
      itemName: "Payment Settings",
      linkTo: "",
    },
    {
      itemName: "Social Login",
      linkTo: "",
    },
    {
      itemName: "Google",
      linkTo: "",
    },
  ]);

  const [NotadminmenuItem] = useState([
    {
      itemName: "Profile",
      linkTo: "",
    },
    {
      itemName: "Account",
      linkTo: "",
    },
    {
      itemName: "Privacy & Safety",
      linkTo: "",
    },
    {
      itemName: "Dashboard",
      linkTo: "",
    },
    {
      itemName: "Referalls",
      linkTo: "",
    },
    {
      itemName: "Bank Details",
      linkTo: "",
    },
    {
      itemName: "Languages",
      linkTo: "",
    },
    {
      itemName: "Logout",
      linkTo: "",
    },
  ]);

  const [data] = useState([
    {
      img: "",
      Name: "Men",
      SubTo: "Admin",
      Date: "Nav 29,2020",
      address: "@Joshtakesoff",
    },
    {
      img: "",
      Name: "Yogi",
      SubTo: "Admin",
      Date: "Nav 29,2020",
      address: "@Joshtakesoff",
    },
    {
      img: "",
      Name: "Men",
      SubTo: "Admin",
      Date: "Nav 29,2020",
      address: "@Joshtakesoff",
    },
    {
      img: "",
      Name: "Men",
      SubTo: "Admin",
      Date: "Nav 29,2020",
      address: "@Joshtakesoff",
    },
    {
      img: "",
      Name: "Men",
      SubTo: "Admin",
      Date: "Nav 29,2020",
      address: "@Joshtakesoff",
    },
    {
      img: "",
      Name: "Men",
      SubTo: "Admin",
      Date: "Nav 29,2020",
      address: "@Joshtakesoff",
    },
  ]);
  const [ViewAll, setViewAll] = useState(false);
  const [ViewAll2, setViewAll2] = useState(false);
  const ShowArray = () => {
    if (ViewAll) {
      return data;
    } else {
      return data.slice(0, 3);
    }
  };

  const ShowArray2 = () => {
    if (ViewAll2) {
      return data;
    } else {
      return data.slice(0, 4);
    }
  };

  const ShowMenu = () => {
    if (isAdmin) {
      return adminmenuItem;
    } else {
      return NotadminmenuItem;
    }
  };
  return (
    <>
      <Nav />
      <div className={classes.container}>
        <div className={classes.sidebarcontainer}>
          <Header
            imgLeft="/img/arrow-left.png"
            heading="Profile"
            backTo="/homepage"
            forwardTo="/dashboard"
          />
          <div className={classes.MenuList}>
            {ShowMenu().map((item, index) => {
              return (
                <Sidebar
                  key={index}
                  item={item}
                  index={index}
                  dashboardItem={dashboardItem}
                />
              );
            })}
          </div>
        </div>
        <div className={classes.AdminDBcontainer}>
          <Header
            imgLeft="/img/arrow-left.png"
            heading={isAdmin ? "Admin Dashboard" : "Dashboard"}
            heading2={isAdmin ? null : "Statistics and balance of your account"}
            backTo="/homepage"
            forwardTo="/dashboard"
          />
          <div className={classes.Tab1}>
            <div className={`${classes.card}`}>
              <img src="/dashboard1.png" alt="" className={classes.img1} />
              <div className={classes.cardtext}>
                <div>36</div>
                <div>Subscriptions</div>
              </div>
              <div className={classes.cardButton}>
                <span>View more..</span>
                <img src="/img/arrow-right.png" alt="" />
              </div>
            </div>
            <div className={classes.card}>
              <img src="/dashboard2.png" alt="" className={classes.img1} />
              <div className={classes.cardtext}>
                <div>$105.90</div>
                <div>Earning Net</div>
              </div>
              <div className={classes.cardButton}>
                <span>Earnings Net</span>
                <img src="/img/arrow-right.png" alt="" />
              </div>
            </div>
            <div className={classes.card}>
              <img src="/dashboard3.png" alt="" className={classes.img1} />
              <div className={classes.cardtext}>
                <div>19</div>
                <div>Members</div>
              </div>
              <div className={classes.cardButton}>
                <span>View more..</span>
                <img src="/img/arrow-right.png" alt="" />
              </div>
            </div>
            <div className={classes.card}>
              <img src="/dashboard4.png" alt="" className={classes.img1} />
              <div className={classes.cardtext}>
                <div>36</div>
                <div>Posts</div>
              </div>
              <div className={classes.cardButton}>
                <span>View more..</span>
                <img src="/img/arrow-right.png" alt="" />
              </div>
            </div>
          </div>
          <div className={classes.Tab2}>
            <div className={classes.tabHeader}>
              Statistics of this month (Feb)
            </div>
            <div className={classes.Tabcontent2}>
              <div>
                <div>$0.00</div>
                <div>Revenue Today</div>
              </div>
              <div>
                <div>$3.00</div>
                <div>Revenue This Week</div>
              </div>
              <div>
                <div>$9.00</div>
                <div>Revenue This Month</div>
              </div>
            </div>
          </div>
          <div className={classes.chartHandler}>
            <Chart />
          </div>
          {isAdmin ? (
            <>
              <div className={classes.MapHandler}>
                <Leafletmap />
              </div>
              <div className={classes.Tab3Handler}>
                <div className={classes.SubCol}>
                  <div className={classes.Tab3Head}>Recent Subscriptions</div>
                  {ShowArray().map((item, index) => {
                    return (
                      <div className={classes.Tab3Card1} key={index}>
                        <Avatar src={item.img} />
                        <div className={classes.text}>
                          <div className={classes.texth1}>
                            {console.log("Iam called")}
                            <span>{item.Name}</span> subscribed to
                            <span>{item.SubTo}</span>
                          </div>
                          <div className={classes.texth2}>{item.Date}</div>
                        </div>
                      </div>
                    );
                  })}
                  <div
                    className={
                      data.length > 3 ? classes.ViewAll : classes.HideViewAll
                    }
                    onClick={() => setViewAll(!ViewAll)}
                  >
                    <span>{ViewAll ? "Show Less" : "View All"}</span>
                    &nbsp;&nbsp;
                    <img src="/arrow-gray.png" alt="" />
                  </div>
                </div>
                <div className={classes.MembersCol}>
                  <div className={classes.Tab3Head}>Latest Members</div>
                  {ShowArray2().map((item, index) => {
                    return (
                      <div className={classes.Tab3Card2} key={index}>
                        <Avatar src={item.img} />
                        <div className={classes.text}>
                          <div className={classes.texth1}>{item.Name}</div>
                          <div className={classes.texth2}>
                            {item.address} / {item.Date}
                          </div>
                        </div>
                        <button className={classes.ActiveButton}>Active</button>
                      </div>
                    );
                  })}
                  <div
                    className={
                      data.length > 3 ? classes.ViewAll : classes.HideViewAll
                    }
                    onClick={() => setViewAll2(!ViewAll2)}
                  >
                    <span>{ViewAll2 ? "Show Less" : "View All"}</span>
                    &nbsp;&nbsp;
                    <img src="/arrow-gray.png" alt="" />
                  </div>
                </div>
              </div>
            </>
          ) : null}
        </div>
      </div>
      <Footer Profile={true} />
    </>
  );
};
