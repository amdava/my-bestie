import React from "react";
import { Nav } from "../../component/nav/pages/nav";
import { Footer } from "../../component/Footer/Footer";
import { Profile } from "./profile/profile";
export const ProfilePage = () => {
  return (
    <>
      <Nav />
      <Profile />
      <Footer
        Home={false}
        Explore={true}
        NewPost={false}
        Msg={false}
        Profile={false}
      />
    </>
  );
};
