import React, { useState } from "react";
import { Profile } from "../profile/profile";
import {
  Row,
  Col,
  Form,
  Input,
  Select,
  Button,
  notification,
  Avatar,
} from "antd";
import { DownOutlined } from "@ant-design/icons";
import { Header } from "../../../mobileReuseable/blueheader/header";
import { Footer } from "../../../component/Footer/Footer";
import { BackDrop } from "../../../component/backdrop/backdrop";
import { Nav } from "../../../component/nav/pages/nav";
import classes from "./post.module.scss";
import "./buttonstyle.css";
import { EditableTagGroup } from "./tags";
import { store, storage } from "../../../firebase";
import { useSelector } from "react-redux";
const { Option } = Select;

export const NewPost = () => {
  const { data } = useSelector((state) => state.UserReducer);
  const [imgUrl, setimgUrl] = useState();
  const [loading, setloading] = useState(false);

  const onSubmitForm = (value) => {
    // console.log(value);
    var date = new Date();

    var months = [
      "Jan",
      "Feb",
      "March",
      "April",
      "May",
      "June",
      "July",
      "Aug",
      "Sept",
      "Oct",
      "Nov",
      "Dec",
    ];
    store
      .collection("posts")
      .doc(data.uid + Date.now())
      .set({
        ...value,
        id: data.uid + Date.now(),
        uid: data.uid,
        email: data.email,
        avatar: data.photo,
        createdAt: months[date.getMonth()] + " " + date.getDate(),
        createdBy: data.userName,
        img: imgUrl,
      })
      .then((docRef) => {
        console.log(docRef);
        notification.success({
          message: "Notification",
          description:
            "success: You've successfully added the post to your timeline.",
          placement: "bottomRight",
        });
      });
  };

  const OnUploadImage = (e) => {
    e.preventDefault();
    var file = e.target.files[0];
    if (file !== undefined) {
      setloading(true);
      var storageref = storage.ref(`posts/${data.id}/${file.name + 1}`);
      var task = storageref.put(file);
      // console.log(imgurl)
      task.on(
        "state_changed",
        (snapshot) => {
          //progresss
        },
        (error) => {
          //incomplete
        },
        async () => {
          await storageref.getDownloadURL().then((url) => setimgUrl(url));
          setloading(false);
        }
      );
    }
  };

  const Reuseabledata = (
    <>
      <Header
        heading="New Posts"
        imgLeft="/img/arrow-left.png"
        alt=""
        backTo="/profile"
        forwardTo="/profile"
      />
      <Row className={classes.mobileInfo} justify="center">
        <Col className={classes.mobileInfoCol}>
          <span>150</span>
          <span>Photo</span>
        </Col>
        <Col className={classes.mobileInfoCol}>
          <span>321</span>
          <span>Video</span>
        </Col>
        <Col className={classes.mobileInfoCol}>
          <span>
            <img src="img/stream-icon.png" alt="" />
          </span>
          <span style={{ color: "#51F0B0", borderBottom: "4px solid #51F0B0" }}>
            Stream
          </span>
        </Col>
      </Row>

      <Form name="imageinfo" onFinish={onSubmitForm}>
        <BackDrop loading={loading} />
        <div className={classes.StreamFormCss}>
          <div className={classes.StreamFormLabel}>Photo Title</div>
          <Form.Item name="title">
            <Input className={classes.inputStream} placeholder="Post title" />
          </Form.Item>

          <div className={classes.StreamFormLabel}>Photo</div>
          <div className="flex items-center mb-10">
            <Avatar
              src={imgUrl}
              alt=""
              style={{ width: "100px", height: "100px" }}
            ></Avatar>
            <div className="upload-btn-wrapper mt-5">
              <Button className="btn ml-3" type="primary">
                Upload
              </Button>
              <input type="file" name="myfile" onChange={OnUploadImage} />
            </div>
          </div>

          <div className={classes.StreamFormLabel}>Details</div>
          <Form.Item name="text">
            <Input.TextArea
              className={classes.inputStream}
              placeholder="Post details"
            />
          </Form.Item>

          <div className={classes.StreamFormLabel}>Privacy</div>
          <Form.Item name="Privacy">
            <Select
              className={classes.inputStream}
              style={{
                border: "none",
                borderBottom: "2px solid #DDDDDD",
                textAlign: "left",
              }}
              bordered={false}
              suffixIcon={
                <DownOutlined style={{ color: "#51F0B0", fontSize: "16px" }} />
              }
              optionLabelProp="label"
              defaultValue={<div className={classes.SelctText}>Public</div>}
            >
              <Option value="Public">Public</Option>
              <Option value="Private">Private</Option>
            </Select>
          </Form.Item>

          <div className={classes.StreamFormLabel}>Location</div>
          <Form.Item name="location">
            <Select
              className={classes.inputStream}
              style={{
                border: "none",
                borderBottom: "2px solid #DDDDDD",
                textAlign: "left",
              }}
              bordered={false}
              //   allowClear
              suffixIcon={
                <DownOutlined style={{ color: "#51F0B0", fontSize: "16px" }} />
              }
              optionLabelProp="label"
              defaultValue={
                <div className={classes.SelctText}>New York, United States</div>
              }
            >
              <Option value="Vietnam">Vietnam</Option>
              <Option value="Russia">Russia</Option>
            </Select>
          </Form.Item>
          {/* <div className={classes.StreamFormLabel}>Tags</div>
          <Form.Item>
            <EditableTagGroup />
          </Form.Item> */}
        </div>
        <Form.Item>
          <Button className={classes.ButtonStyle} htmlType="submit">
            <img src="/img/confirm.png" alt="" />
            &nbsp;&nbsp;
            <span>SAVE CHANGES</span>
          </Button>
        </Form.Item>
      </Form>

      {/* <Form name="streaminfo" onFinish={onSubmitForm}>
        <div className={classes.StreamFormCss}>
          <div className={classes.StreamFormLabel}>Stream Title</div>
          <Form.Item name="title">
            <Input
              className={classes.inputStream}
              placeholder="My First Stream"
            />
          </Form.Item>
          <div className={classes.StreamFormLabel}>Privacy</div>
          <Form.Item name="Privacy">
            <Select
              className={classes.inputStream}
              style={{
                border: "none",
                borderBottom: "2px solid #DDDDDD",
                textAlign: "left",
              }}
              bordered={false}
              suffixIcon={
                <DownOutlined style={{ color: "#51F0B0", fontSize: "16px" }} />
              }
              optionLabelProp="label"
              defaultValue={<div className={classes.SelctText}>Public</div>}
            >
              <Option value="Public">Public</Option>
              <Option value="Private">Private</Option>
            </Select>
          </Form.Item>

          <div className={classes.StreamFormLabel}>Location</div>
          <Form.Item name="location">
            <Select
              className={classes.inputStream}
              style={{
                border: "none",
                borderBottom: "2px solid #DDDDDD",
                textAlign: "left",
              }}
              bordered={false}
              //   allowClear
              suffixIcon={
                <DownOutlined style={{ color: "#51F0B0", fontSize: "16px" }} />
              }
              optionLabelProp="label"
              defaultValue={
                <div className={classes.SelctText}>New York, United States</div>
              }
            >
              <Option value="Vietnam">Vietnam</Option>
              <Option value="Russia">Russia</Option>
            </Select>
          </Form.Item>
          <div className={classes.StreamFormLabel}>Tags</div>
          <Form.Item>
            <EditableTagGroup />
          </Form.Item>
        </div>
        <Form.Item>
          <Button className={classes.ButtonStyle} htmlType="submit">
            <img src="/img/confirm.png" alt="" />
            &nbsp;&nbsp;
            <span>SAVE CHANGES</span>
          </Button>
        </Form.Item>
      </Form> */}
    </>
  );
  return (
    <>
      <BackDrop loading={loading} />

      <div className={classes.container}>
        <Nav />
        <div className={classes.DesktopStreamContainer}>
          <div>
            <Profile newpost={true} />
          </div>
          <div>{Reuseabledata}</div>
        </div>
        <div className={classes.MobileStreamContainer}>
          <div>{Reuseabledata}</div>
        </div>
        <Footer
          Home={false}
          Explore={false}
          NewPost={true}
          Msg={false}
          Profile={false}
        />
      </div>
    </>
  );
};
