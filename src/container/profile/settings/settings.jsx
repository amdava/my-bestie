import classes from "./settings.module.scss";
import React, { useState } from "react";
import { Header } from "../../../mobileReuseable/blueheader/header";
import { useHistory, NavLink } from "react-router-dom";
import { EditProfileContent } from "./components/EditProfile/EditProfileContent";
import { BankDetailsContent } from "./components/BankDetails/BankDetailsContent";
import { ReferallsContent } from "./components/Referalls/ReferallsContent";
import { BankAddStarContent } from "./components/BankAddStars/BankAddStarContent";
import { SendAddStarContent } from "./components/SendAddStars/SendAddStarContent";
import { Nav } from "../../../component/nav/pages/nav";
import { Footer } from "../../../component/Footer/Footer";
import { useAuth } from "../../../contexts/AuthContext";
import { message } from "antd";

export const Settings = ({
  Logout,
  EditPage,
  BankPage,
  BankAddStarsPage,
  SendAddStarsPage,
  RefPage,
  PageOpened,
}) => {
  const history = useHistory();
  const [error, seterror] = useState("");
  const [Loading, setLoading] = useState(false);
  const { logout } = useAuth();
  const onFinish = async () => {
    try {
      seterror("");
      setLoading(true);
      await logout();
      message.success("Success: You log out successfully.");
      // setLoading(false);
      // history.push("/homepage");
    } catch (error) {
      seterror("Failed to log out");
      setLoading(false);
    }
  };

  const arrowImageWhite = (
    <img src="/img/arrow-icon-white.png" alt="" width="15px" height="23px" />
  );

  const arrowImageGreen = (
    <img src="/img/arrow-icon-green.png" alt="" width="15px" height="23px" />
  );
  return (
    <>
      <Nav />
      <div className={classes.SettingsContainer}>
        <div
          className={
            PageOpened ? classes.SettingsViewer1 : classes.SettingsProfileView
          }
        >
          {error ? message.success({ error }) : null}
          <Header
            imgLeft="/img/arrow-left.png"
            heading="Profile"
            backTo="/homepage"
            forwardTo="/settings"
          />
          <div className={classes.SettingsContent}>
            <NavLink
              exact
              to="/settings/edit-profile"
              activeClassName={classes.SettingsTabActive}
              className={classes.SettingsTab}
            >
              <span>Profile</span>
              <>
                {history.location.pathname === "/settings/edit-profile"
                  ? arrowImageWhite
                  : arrowImageGreen}
              </>
            </NavLink>
            <NavLink
              exact
              to="/dashboard"
              activeClassName={classes.SettingsTabActive}
              className={classes.SettingsTab}
            >
              <span>Account</span>
              <>
                {history.location.pathname === "/dashboard"
                  ? arrowImageWhite
                  : arrowImageGreen}
              </>
            </NavLink>
            <NavLink
              exact
              to="/settings/privacy"
              activeClassName={classes.SettingsTabActive}
              className={classes.SettingsTab}
            >
              <span>Privacy & Safety</span>
              <>
                {history.location.pathname === "/settings/privacy"
                  ? arrowImageWhite
                  : arrowImageGreen}
              </>
            </NavLink>
            <NavLink
              exact
              to="/settings/become-creator"
              activeClassName={classes.SettingsTabActive}
              className={classes.SettingsTab}
            >
              <span>Become a Creator</span>
              <>
                {history.location.pathname === "/settings/become-creator"
                  ? arrowImageWhite
                  : arrowImageGreen}
              </>
            </NavLink>
            <NavLink
              exact
              to="/settings/referalls"
              activeClassName={classes.SettingsTabActive}
              className={classes.SettingsTab}
            >
              <span>Referalls</span>
              <>
                {history.location.pathname === "/settings/referalls"
                  ? arrowImageWhite
                  : arrowImageGreen}
              </>
            </NavLink>
            <NavLink
              exact
              to="/settings/display"
              activeClassName={classes.SettingsTabActive}
              className={classes.SettingsTab}
            >
              <span>Display</span>
              <>
                {history.location.pathname === "/settings/display"
                  ? arrowImageWhite
                  : arrowImageGreen}
              </>
            </NavLink>
            <NavLink
              exact
              to="/settings/bank-details"
              activeClassName={classes.SettingsTabActive}
              className={classes.SettingsTab}
            >
              <span>Bank Details</span>
              <>
                {history.location.pathname === "/settings/bank-details"
                  ? arrowImageWhite
                  : arrowImageGreen}
              </>
            </NavLink>
            <NavLink
              exact
              to="/settings/sendstars"
              activeClassName={classes.SettingsTabActive}
              className={classes.SettingsTab}
            >
              <span>Languages</span>
              <>
                {history.location.pathname === "/settings/sendstars"
                  ? arrowImageWhite
                  : arrowImageGreen}
              </>
            </NavLink>

            <button
              className={
                Logout ? classes.SettingsTabActive : classes.SettingsTab
              }
              onClick={onFinish}
              disabled={Loading}
              style={{ cursor: "pointer" }}
            >
              <span>Logout</span>
              <>{Logout ? arrowImageWhite : arrowImageGreen}</>
            </button>
          </div>
        </div>
        <div className={PageOpened ? classes.SettingsViewer2 : null}>
          {EditPage ? <EditProfileContent /> : null}
          {BankPage ? <BankDetailsContent /> : null}
          {RefPage ? <ReferallsContent /> : null}
          {BankAddStarsPage ? <BankAddStarContent /> : null}
          {SendAddStarsPage ? <SendAddStarContent /> : null}
        </div>
      </div>
      <Footer />
    </>
  );
};
