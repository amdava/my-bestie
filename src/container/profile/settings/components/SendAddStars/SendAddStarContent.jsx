import classes from "./SendAddStars.module.scss";
import React, { useState } from "react";
import { Button, Avatar, Input } from "antd";
import { Link } from "react-router-dom";

export const SendAddStarContent = () => {
	const [stars, setstars] = useState(0);
	const [comment, setcomment] = useState("");
	const OnSubmitForm = () => {
		console.log(stars, comment);
	};

	const CustomValue = (e) => {
		console.log("stars", e.target.value);
		setstars(e.target.value);
	};

	const ActiveButton = (value) => {
		console.log("stars", value);
		setstars(value);
	};

	const BuyFunction = () => {
		console.log("Buy button called");
	};
	const CommentHandler = (e) => {
		console.log("comment", e.target.value);
		setcomment(e.target.value);
	};
	return (
		<div style={{ paddingBottom: "100px" }}>
			<header>
				<Avatar
					className={classes.Avatar}
					src="https://static.remove.bg/remove-bg-web/bf023cd87829b02caf45d8634cbf60bd4529e647/assets/start-0e837dcc57769db2306d8d659f53555feb500b3c5d456879b9c843d1872e7baa.jpg"
					alt=""
				/>
				<Link to="/settings">
					<img src="/img/arrow-left.png" alt="" className={classes.arrowLeft} />
				</Link>
				<div className={classes.ButtonsHandler}>
					<Button className={classes.BuyButton} onClick={BuyFunction}>
						<img src="/img/case.png" alt="" />
						&nbsp;<span>Buy</span>
					</Button>
					&nbsp;&nbsp;&nbsp;
					<Button className={classes.StarButton}>
						<img src="/img/star.png" alt="" />
						&nbsp;<span>{stars}</span>
					</Button>
				</div>
				<div className={classes.text}>
					<div>Send Stars to Support</div>
					<div>Dina</div>
				</div>
			</header>
			<div className={classes.container}>
				<div className={classes.card} onClick={() => setstars(99)}>
					<div className={classes.heading}>Special Star Offer</div>
					<div className={classes.cardtext}>
						Buy Stars at a special, discounted price for a limited time only.
					</div>
					<button className={`${classes.CardButton} flex items-center`}>
						<span>99 Stars</span>
						<span className="flex items-center">
							<img src="/img/star.png" alt="" />
							<span>Limited time only</span>
						</span>
						<span>$0.99</span>
					</button>
				</div>
				<div className={classes.StarsRow}>
					<Button
						className={stars === 50 ? classes.ActiveStarButton : classes.StarButton}
						onClick={() => ActiveButton(50)}
					>
						{stars === 50 ? (
							<img src="/img/bluestar.png" alt="" />
						) : (
							<img src="/img/graystar.png" alt="" />
						)}
						&nbsp;<span>50</span>
					</Button>
					<Button
						className={stars === 100 ? classes.ActiveStarButton : classes.StarButton}
						onClick={() => ActiveButton(100)}
					>
						{stars === 100 ? (
							<img src="/img/bluestar.png" alt="" />
						) : (
							<img src="/img/graystar.png" alt="" />
						)}
						&nbsp;<span>100</span>
					</Button>
					<Button
						className={stars === 200 ? classes.ActiveStarButton : classes.StarButton}
						onClick={() => ActiveButton(200)}
					>
						{stars === 200 ? (
							<img src="/img/bluestar.png" alt="" />
						) : (
							<img src="/img/graystar.png" alt="" />
						)}
						&nbsp;<span>200</span>
					</Button>
				</div>
				<div className={classes.StarsRow}>
					<Button
						className={stars === 500 ? classes.ActiveStarButton : classes.StarButton}
						onClick={() => ActiveButton(500)}
					>
						{stars === 500 ? (
							<img src="/img/bluestar.png" alt="" />
						) : (
							<img src="/img/graystar.png" alt="" />
						)}
						&nbsp;<span>500</span>
					</Button>
					<Button
						className={stars === 1000 ? classes.ActiveStarButton : classes.StarButton}
						onClick={() => ActiveButton(1000)}
					>
						{stars === 1000 ? (
							<img src="/img/bluestar.png" alt="" />
						) : (
							<img src="/img/graystar.png" alt="" />
						)}
						&nbsp;<span>1000</span>
					</Button>
					<Button
						className={stars === 2000 ? classes.ActiveStarButton : classes.StarButton}
						onClick={() => ActiveButton(2000)}
					>
						{stars === 2000 ? (
							<img src="/img/bluestar.png" alt="" />
						) : (
							<img src="/img/graystar.png" alt="" />
						)}
						&nbsp;<span>2000</span>
					</Button>
				</div>
				<Input
					className={classes.InputHandler}
					prefix={<img src="/img/graystar.png" alt="" />}
					placeholder="Custom"
					onChange={(e) => CustomValue(e)}
				/>

				<div className={classes.userSendData}>
					<Avatar
						className={classes.userAvatar}
						src="https://static.remove.bg/remove-bg-web/bf023cd87829b02caf45d8634cbf60bd4529e647/assets/start-0e837dcc57769db2306d8d659f53555feb500b3c5d456879b9c843d1872e7baa.jpg"
						alt=""
					/>
					<div className={classes.ButtonHandler}>
						<Input placeholder="Add a comment with Stars" onChange={CommentHandler} />
						<Button  onClick={() => setstars(50)} className="flex items-center">
							<img src="/img/blackstar.png" alt="" />
							&nbsp;<span>Sent 50 Stars</span>
						</Button>
					</div>
				</div>
			</div>
			<Button className={classes.ButtonStyle} onClick={OnSubmitForm}>
				<span>GET STARS</span>
			</Button>
		</div>
	);
};
