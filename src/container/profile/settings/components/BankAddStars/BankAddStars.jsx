import React from "react";
import { Settings } from "../../settings";
export const BankAddStarPage = () => {
  return (
    <Settings
      BankDetails={true}
      BankAddStarsPage={true}
      PageOpened={true}
    ></Settings>
  );
};
