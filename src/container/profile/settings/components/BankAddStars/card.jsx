import classes from "./BankAddStars.module.scss";
import React from "react";

export const Card = ({ stars, dollar, border, bg , AddStars }) => {
  return (
    <div
      className={classes.card}
      onClick={AddStars}
      style={{ backgroundColor: bg, border: `1px solid ${border}` }}
    >
      <span>{stars} stars Stars</span>
      <span>${dollar}</span>
    </div>
  );
};
