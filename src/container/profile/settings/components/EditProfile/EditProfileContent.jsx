import classes from "./EditProfile.module.scss";
import React, { useState } from "react";
import { Header } from "../../../../../mobileReuseable/blueheader/header";
import { Button, Input } from "antd";
export const EditProfileContent = () => {
  const [UserData, setUserData] = useState({
    FullName: "Aron Smith",
    Country: "United Kingdom",
    Email: "Aaron.smith@email.com",
  });
  const [EditRow_1, setEditRow_1] = useState(false);
  const [EditRow_2, setEditRow_2] = useState(false);
  const [EditRow_3, setEditRow_3] = useState(false);

  const OnSubmitForm = () => {
    setEditRow_1(false);
    setEditRow_2(false);
    setEditRow_3(false);
    console.log(UserData);
  };

  return (
    <>
      <Header imgRight="/img/x.png" heading="Edit Profile" forwardTo="/settings" backTo="/settings"/>
      <div className={classes.EditProfileContainer}>
        <div className={classes.EditProfileRow}>
          <div className={classes.EditProfileCol}>
            <span className={classes.EditProfileColHead}>Full Name</span>
            <span className={classes.EditProfileColData}>
              {UserData.FullName}
            </span>
          </div>
          <div className={classes.EditProfileCol}>
            <span className={classes.EditProfileColHead}>Full Name</span>
            <span className={classes.EditProfileColData}>
              {EditRow_1 ? (
                <Input
                  className={classes.EditProfileColData}
                  value={UserData.FullName}
                  onChange={(e) => {
                    const data = { ...UserData };
                    data.FullName = e.target.value;
                    setUserData(data);
                  }}
                />
              ) : (
                <span className={classes.DefaultValue}>
                  {UserData.FullName}
                </span>
              )}
              &nbsp;
              <img
                src="/img/edit.png"
                alt="edit.png"
                width="34.37px"
                height="28.94"
                onClick={() => setEditRow_1(true)}
              />
            </span>
          </div>
        </div>
        <div className={classes.EditProfileRow}>
          <div className={classes.EditProfileCol}>
            <span className={classes.EditProfileColHead}>Country</span>
            <span className={classes.EditProfileColData}>
              {UserData.Country}
            </span>
          </div>
          <div className={classes.EditProfileCol}>
            <span className={classes.EditProfileColHead}>Country</span>
            <span className={classes.EditProfileColData}>
              {EditRow_2 ? (
                <Input
                  className={classes.EditProfileColData}
                  value={UserData.Country}
                  onChange={(e) => {
                    const data = { ...UserData };
                    data.Country = e.target.value;
                    setUserData(data);
                  }}
                />
              ) : (
                <span className={classes.DefaultValue}>{UserData.Country}</span>
              )}
              &nbsp;
              <img
                src="/img/edit.png"
                alt="edit.png"
                width="34.37px"
                height="28.94"
                onClick={() => setEditRow_2(true)}
              />
            </span>
          </div>
        </div>
        <div className={classes.EditProfileRow}>
          <div className={classes.EditProfileCol}>
            <span className={classes.EditProfileColHead}>Email</span>
            <span className={classes.EditProfileColData}>{UserData.Email}</span>
          </div>
          <div className={classes.EditProfileCol}>
            <span className={classes.EditProfileColHead}>Email</span>
            <span className={classes.EditProfileColData}>
              {EditRow_3 ? (
                <Input
                  className={classes.EditProfileColData}
                  value={UserData.Email}
                  onChange={(e) => {
                    const data = { ...UserData };
                    data.Email = e.target.value;
                    setUserData(data);
                  }}
                />
              ) : (
                <span className={classes.DefaultValue}>{UserData.Email}</span>
              )}
              &nbsp;
              <img
                src="/img/edit.png"
                alt="edit.png"
                width="34.37px"
                height="28.94"
                onClick={() => setEditRow_3(true)}
              />
            </span>
          </div>
        </div>
        <Button className={classes.ButtonStyle} onClick={OnSubmitForm}>
          <img src="/img/confirm.png" alt="" />
          &nbsp;&nbsp;
          <span>SAVE CHANGES</span>
        </Button>
      </div>
    </>
  );
};
