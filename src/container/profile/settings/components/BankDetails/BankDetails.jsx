import React from "react";
import { Settings } from "../../settings";
export const BankPage = () => {
  return (
    <Settings
      BankDetails={true}
      BankPage={true}
      PageOpened={true}
    ></Settings>
  );
};
