import React from "react";
import { Header } from "../../../../../mobileReuseable/blueheader/header";
import { Button } from "antd";
import classes from "../../settings.module.scss";
export const BankDetailsContent = () => {
  return (
    <>
      <Header
        imgRight="/img/plus.png"
        heading="Bank Details"
        backTo="/settings"
        forwardTo="/settings/add-stars-to-balance"
      />
      <div className={classes.CardHandler}>
        <div className={classes.CardContent}>
          <img src="/card1.png" alt="" />
          <div className={classes.CardInfo}>
            <div>Saved Card</div>
            <div>●●●● ●●●● ●●●● 5678</div>
          </div>
        </div>
        <img src="/img/arrow-icon.png" alt="" className={classes.Arrow}/>
      </div>
      <div className={classes.CardHandler}>
        <div className={classes.CardContent}>
          <img src="/card2.png" alt="" />
          <div className={classes.CardInfo}>
            <div>Saved Card</div>
            <div>●●●● ●●●● ●●●● 5678</div>
          </div>
        </div>
        <img src="/img/arrow-icon.png" alt="" className={classes.Arrow}/>
      </div>
      <div className={classes.CardHandler}>
        <div className={classes.CardContent}>
          <img src="/card3.png" alt="" />
          <div className={classes.CardInfo}>
            <div>Saved Card</div>
            <div>●●●● ●●●● ●●●● 5678</div>
          </div>
        </div>
        <img src="/img/arrow-icon.png" alt="" className={classes.Arrow}/>
      </div>
      <Button className={classes.ButtonStyle}>
        <img src="/img/confirm.png" alt="" />
        &nbsp;&nbsp;
        <span>SAVE CHANGES</span>
      </Button>
    </>
  );
};
