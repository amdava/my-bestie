import React from "react";
import { Avatar } from "antd";
import classes from "../style.module.scss";
import { useHistory } from "react-router-dom";
export const Chat = ({ item, textcolor, bg, chatcolor, index }) => {
  const { messages, recieverHasRead, sender } = item;
  // const { UserName, Msg, Time, Count, avatar } = item;
  const history = useHistory();
  const onChatOpen = (value) => {
    const data = JSON.stringify(value);
    localStorage.setItem("chatOpened", data);
    // history.push("/messages/chat");
  };

  // console.log(item);
  return (
    <div
      className={classes.ChatHandler}
      style={{ backgroundColor: bg }}
      onClick={() => onChatOpen(item)}
    >
      <Avatar
        // src={avatar}
        className={classes.Avatar}
        style={{ border: `3px solid ${chatcolor}` }}
      />
      <div
        className={classes.bghandler}
        style={{ backgroundColor: bg, zIndex: -index }}
      ></div>
      <span style={{ color: textcolor }} className={classes.UserName}>
        {/* {UserName} */}
        {messages[messages.length - 1].sender}
      </span>
      <span style={{ color: chatcolor }} className={classes.Msg}>
        {/* {Msg.substring(0, 50) + "..."} */}
        {messages[messages.length - 1].message.substring(0, 30)}
      </span>
      <span style={{ color: textcolor }} className={classes.Time}>
        {/* {Time} */}
      </span>
      {/* {Count > 0 ? <div className={classes.Count}>{Count}</div> : null} */}
    </div>
  );
};
