import React, { useState, useEffect } from "react";
// import { Messages } from "../messages";
import { Input, Button, Avatar, Row } from "antd";
import { Nav } from "../../../component/nav/pages/nav";
import { Footer } from "../../../component/Footer/Footer";
import classes from "./chatopened.module.scss";
import mclasses from "../style.module.scss";
import { useHistory } from "react-router-dom";
import { Header } from "../../../mobileReuseable/blueheader/header";
import { store } from "../../../firebase";
import { Chat } from "./chat";
import firebase from "firebase/app";
import { useSelector } from "react-redux";
import { BackDrop } from "../../../component/backdrop/backdrop";
import NewChatComponent from "./newChat";
export const Chatopened = () => {
	const history = useHistory();
	const { data, loading } = useSelector((state) => state.UserReducer);
	const [dualscreen, setdualscreen] = useState(false);

	const [chatter, setchatter] = useState();
	const [selectedChat, setselectedChat] = useState();
	const [newChat, setnewChat] = useState(false);
	// JSON.parse(localStorage.getItem("chatOpened"))

	const [msgTyped, setmsgTyped] = useState("");
	const [chat, setchat] = useState([]);

	useEffect(() => {
		const container = document.getElementById("auto-scroll-container");
		if (container) container.scrollTo(0, container.scrollHeight);
	});

	useEffect(() => {
		if ((loading === false) & !isEmpty(data)) {
			store
				.collection("chats")
				.where("users", "array-contains", data.email.toLowerCase())
				.onSnapshot((res) => {
					const chats = res.docs.map((doc) => doc.data()); 
					setchat(chats);
				});
		}
	}, [loading]);

	function isEmpty(obj) {
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) return false;
		}
		return true;
	}

	const OnEnterMessage = () => {
		console.log("chat", msgTyped);
		SubmitFn(msgTyped);
	};

	const OnMoveToPage = (address) => {
		history.push(address);
	};

	const userTyping = (e) => {
		setmsgTyped(e.target.value);
		messageRead(selectedChat);
	};

	const userClickedInput = () => {
		console.log("UserClickedInput");
	};

	const onNewMessage = () => {
		setnewChat(true);
		setdualscreen(true);
		console.log("new Message");
	};

	const buildDocKey = (friend) => [data.email.toLowerCase(), friend].sort().join(":");

	const SubmitFn = () => {
		const dockey = buildDocKey(
			chat[selectedChat].users.filter((_usr) => _usr !== data.email.toLowerCase())[0]
		);

		store
			.collection("chats")
			.doc(dockey)
			.update({
				messages: firebase.firestore.FieldValue.arrayUnion({
					sender: data.email,
					message: msgTyped,
					timestamp: Date.now(),
				}),
				recieverHasRead: false,
			});
	};

	const OnOpenChatFn = async (index, item) => {
		await setselectedChat(index);
		console.log(item);
		setchatter(item);
		setdualscreen(true);
		messageRead(index);
		// return index;
	};

	const messageRead = (chatid) => {
		// console.log(chatid);
		const dockey = buildDocKey(
			chat[chatid].users.filter((_usr) => _usr !== data.email.toLowerCase())[0]
		);
		console.log(dockey);
		if (clickedChatWhereNotSender(chatid)) {
			firebase.firestore().collection("chats").doc(dockey).update({
				recieverHasRead: true,
			});
		} else {
			console.log("Clicked message where the user was sender");
		}
	};
	//get the last message sender name / email
	const clickedChatWhereNotSender = (selectedChat) => {
		return (
			chat[selectedChat].messages[chat[selectedChat].messages.length - 1].sender.toLowerCase() !==
			data.email.toLowerCase()
		);
	};
	//user read the mesage to check
	const userIsSender = (chat) => {
		return (
			chat.messages[chat.messages.length - 1].sender.toLowerCase() === data.email.toLowerCase()
		);
	};

	console.log(selectedChat);
	console.log(chat[selectedChat]);

	const selectChat = async (chatIndex) => {
		console.log(chatIndex);
		await setselectedChat(chatIndex);
		await setnewChat(false);
		messageRead(chatIndex);
	};

	const goToChat = async (docKey, msg) => {
		const usersInChat = docKey.split(":");
		const goTochat = chat.find((_chat) =>
			usersInChat.every((_user) => _chat.users.includes(_user.toLowerCase()))
		);
		await selectChat(chat.indexOf(goTochat));
		await setnewChat(false);
		SubmitFn(msg);
		// submitMessage(msg);
	};

	const newChatSubmit = async (chatObj) => {
		const docKey = buildDocKey(chatObj.sendTo);
		await firebase
			.firestore()
			.collection("chats")
			.doc(docKey)
			.set({
				messages: [
					{
						message: chatObj.message,
						sender: data.email.toLowerCase(),
					},
				],
				users: [data.email.toLowerCase(), chatObj.sendTo.toLowerCase()],
				receiverHasRead: false,
			});
		  selectChat(chat.length - 1);
	};
	return (
		<>
			<BackDrop loading={loading} />
			{dualscreen ? (
				<>
					<Nav />
					<div className={classes.chatopened}>
						<div className={classes.Messages}>
							<div
								className={`${
									dualscreen ? mclasses.dualcontainer : mclasses.container
								} min-h-screen`}
							>
								<div style={{ paddingBottom: "100px" }}>
									<Header
										imgLeft="/img/arrow-left.png"
										imgRight="/img/search.png"
										heading="Messages"
										backTo="/notifications"
										// forwardTo="/explore"
									/>
									{chat.map((_item, _index) => {
										return (
											<span onClick={() => OnOpenChatFn(_index, _item)} key={_index}>
												<Chat
													item={_item}
													bg="white"
													textcolor="#998FA2"
													chatcolor="#241332"
													index={_index + 1}
												/>
											</span>
										);
									})}
								</div>
							</div>
						</div>
						{newChat ? (
							<NewChatComponent goToChatFn={goToChat} newChatSubmitFn={newChatSubmit} />
						) : (
							<div className={classes.Chat}>
								<div className={classes.ChatHeader}>
									<div className={classes.HeaderTop}>
										<img
											src="/img/arrow-left.png"
											alt=""
											className={classes.Mobilearrow}
											onClick={() => setdualscreen(false)}
										/>
										<img
											src="/arrow-left.png"
											alt=""
											className={classes.arrow}
											onClick={() => setdualscreen(false)}
										/>
										{/* <div className={classes.Name}>{chatter.UserName}</div> */}
										<img
											src="/img/settings.png"
											alt=""
											className={classes.MobileSettings}
											onClick={() => OnMoveToPage("/settings")}
										/>
										<img src="/settings.png" alt="" className={classes.Settings} />
									</div>
									<div className={classes.ChatHeaderBottom}>
										<div>Last seen Feb 5</div>
										<div className={classes.HeaderIcons}>
											<img src="/star.png" alt="" />
											<img src="/bell.png" alt="" />
											<img src="/image.png" alt="" />
											<img src="/search.png" alt="" />
										</div>
										<div className={classes.MobileIcons}>
											<img src="/whitestar.png" alt="" />
											<img src="/whitebell.png" alt="" />
											<img src="/whiteimage.png" alt="" />
											<img src="/whitesearch.png" alt="" />
										</div>
									</div>
								</div>
								<div className={classes.ChatHandler} id="auto-scroll-container">
									{chat[selectedChat].messages.map((item, index) => {
										return item.sender.toLowerCase() === data.email.toLowerCase() ? (
											<div key={index} className={classes.MessageFrom}>
												<Row className={classes.MessageInfo}>
													<Avatar src={item.Avatar} className={classes.Avatar} />
													<div className={classes.User}>You</div>
												</Row>
												<div className={classes.Msg}>{item.message}</div>
											</div>
										) : (
											<div key={index} className={classes.MessageTo}>
												<Row className={classes.MessageInfo}>
													<Avatar src={item.Avatar} className={classes.Avatar} />
													<div className={classes.User}>{item.sender}</div>
												</Row>
												<div className={classes.Msg}>{item.message}</div>
											</div>
										);
									})}
								</div>
								<div className={classes.FooterHandler}>
									<Input
										placeholder="Type a message"
										id="chattextbox"
										className={classes.Input}
										onChange={(e) => setmsgTyped(e.target.value)}
										onKeyUp={(e) => userTyping(e)}
										onFocus={userClickedInput}
									/>
									<div className={classes.FooterContent}>
										<div>
											<img src="img/explore-6.png" alt="" />
											<img src="img/chat$.png" alt="" />
										</div>
										<Button onClick={OnEnterMessage}>Send</Button>
									</div>
								</div>
							</div>
						)}
					</div>
					<Footer Msg={true} />
				</>
			) : (
				<>
					<Nav />
					<div
						className={`${dualscreen ? mclasses.dualcontainer : mclasses.container} min-h-screen`}
					>
						<Header
							imgLeft="/img/arrow-left.png"
							imgRight="/img/search.png"
							heading="Messages"
							backTo="/notifications"
							// forwardTo="/explore"
						/>
						<Button
							size="large"
							type="primary"
							style={{
								width: "90%",
								borderRadius: "40px",
								fontWeight: "bold",
							}}
							onClick={onNewMessage}
						>
							New Message
						</Button>
						<div style={{ paddingBottom: "100px" }}>
							{chat.map((_item, _index) => {
								return (
									<span onClick={() => OnOpenChatFn(_index, _item)} key={_index}>
										{_item.recieverHasRead === false && !userIsSender(_item) ? (
											<Chat
												item={_item}
												bg="#51F0B0"
												textcolor="white"
												chatcolor="white"
												index={_index + 1}
											/>
										) : (
											<Chat
												item={_item}
												bg="white"
												textcolor="#998FA2"
												chatcolor="#241332"
												index={_index + 1}
											/>
										)}
									</span>
								);
							})}
						</div>
					</div>
					<Footer Msg={true} />
				</>
			)}
		</>
	);
};
