import React, { useState, useEffect } from "react";
import { Button } from "antd";
import { Chat } from "./components/chat";
import { Footer } from "../Footer/Footer";
import { Header } from "../../mobileReuseable/blueheader/header";
import { Nav } from "../nav/pages/nav";
import classes from "./style.module.scss";
import { store } from "../../firebase";
import { useSelector } from "react-redux";
export const Messages = ({ dualscreen }) => {
  // const userData = JSON.parse(localStorage.getItem("userdata"));
  const { data } = useSelector((state) => state.UserReducer);
  // console.log(userData.email);
  const [chat, setchat] = useState([]);
  console.log(data.email);
  useEffect(() => {
    data !== undefined
      ? store
          .collection("chats")
          .where("users", "array-contains", data.email.toLowerCase())
          .onSnapshot((res) => {
            const chats = res.docs.map((doc) => doc.data());

            console.log(chats);
            setchat(chats);
          })
      : null;
  }, []);

  const [msg] = useState([
    {
      avatar:
        "https://media.newyorker.com/photos/5ec2d7a40fe2fbfb61a298c8/4:3/w_1808,h_1356,c_limit/Russell-NormalPeople-3.jpg",
      UserName: "Rubaika",
      Msg: "Welcome to Yoga Meetup",
      Time: "9 hrs",
      Count: 5,
      groupchat: false,
    },
    {
      avatar:
        "https://media.newyorker.com/photos/5ec2d7a40fe2fbfb61a298c8/4:3/w_1808,h_1356,c_limit/Russell-NormalPeople-3.jpg",
      UserName: "yungb00",
      Msg: "Welcome to Yoga Meetup",
      Time: "9 hrs",
      Count: 0,
      groupchat: false,
    },
    {
      avatar:
        "https://media.newyorker.com/photos/5ec2d7a40fe2fbfb61a298c8/4:3/w_1808,h_1356,c_limit/Russell-NormalPeople-3.jpg",
      UserName: "Rubaika",
      Msg: "Hola",
      Time: "9 hrs",
      Count: 2,
      groupchat: true,
    },
    {
      avatar:
        "https://media.newyorker.com/photos/5ec2d7a40fe2fbfb61a298c8/4:3/w_1808,h_1356,c_limit/Russell-NormalPeople-3.jpg",
      UserName: "yungb00",
      Msg: "Welcome to Yoga Meetup",
      Time: "9 hrs",
      Count: 0,
      groupchat: false,
    },
    {
      avatar:
        "https://media.newyorker.com/photos/5ec2d7a40fe2fbfb61a298c8/4:3/w_1808,h_1356,c_limit/Russell-NormalPeople-3.jpg",
      UserName: "yungb00",
      Msg: "Welcome to Yoga Meetup",
      Time: "9 hrs",
      Count: 0,
      groupchat: false,
    },
  ]);

  const onNewMessage = () => {
    console.log("new Message");
  };
  // console.log(chat);

  const buildDocKey = (friend) =>
    [userData.email.toLowerCase(), friend].sort().join(":");

  const SubmitFn = () => {
    const dockey = buildDocKey(
      chat[OnOpenChatFn()].users.filter(
        (_usr) => _usr !== userData.email.toLowerCase()
      )[0]
    );
    console.log(dockey);
  };

  const OnOpenChatFn = (index) => {
    return index;
  };

  return (
    <>
      {dualscreen ? null : <Nav />}
      <div className={dualscreen ? classes.dualcontainer : classes.container}>
        {dualscreen ? null : (
          <Header
            imgLeft="/img/arrow-left.png"
            imgRight="/img/search.png"
            heading="Messages"
            backTo="/notifications"
            forwardTo="/explore"
          />
        )}
        <Button
          size="large"
          type="primary"
          style={{ width: "90%", borderRadius: "40px", fontWeight: "bold" }}
          onClick={onNewMessage}
        >
          New Message
        </Button>
        <div style={{ paddingBottom: "100px" }}>
          {chat.map((_item, _index) => {
            return (
              <span onClick={() => OnOpenChatFn(_index)} key={_index}>
                <Chat
                  item={_item}
                  bg="white"
                  textcolor="#998FA2"
                  chatcolor="#241332"
                  index={_index + 1}
                />
              </span>
            );
          })}
          {/* {msg.map((item, index) => {
            return item.groupchat ? (
              <Chat
                key={index}
                item={item}
                bg="#51F0B0"
                textcolor="white"
                chatcolor="white"
                index={index + 1}
              />
            ) : item.Count > 0 ? (
              <Chat
                key={index}
                item={item}
                bg="#51F0B0"
                textcolor="white"
                chatcolor="white"
                index={index + 1}
              />
            ) : (
              <Chat
                key={index}
                item={item}
                bg="white"
                textcolor="#998FA2"
                chatcolor="#241332"
                index={index + 1}
              />
            );
          })} */}
        </div>
      </div>
      {dualscreen ? null : <Footer Msg={true} />}
    </>
  );
};
