import React, { useState } from "react";
import classes from "./notification.module.scss";
import { Activities } from "./components/activities";
import { Link } from "react-router-dom";
import { Nav } from "../../component/nav/pages/nav";
import { Footer } from "../../component/Footer/Footer";
export const Notifications = () => {
  const [dropdown, setdropdown] = useState(false);
  const [activities] = useState([
    {
      avatar:
        "https://media.newyorker.com/photos/5ec2d7a40fe2fbfb61a298c8/4:3/w_1808,h_1356,c_limit/Russell-NormalPeople-3.jpg",
      isMentioned: true,
      UserName: "Rubaika",
      Activity: "mentioned you in a comment",
      Time: "9 hrs",
    },
    {
      avatar:
        "https://media.newyorker.com/photos/5ec2d7a40fe2fbfb61a298c8/4:3/w_1808,h_1356,c_limit/Russell-NormalPeople-3.jpg",
      isMentioned: false,
      UserName: "yungb00",
      Activity: "send you a message",
      Time: "23 hrs ago",
    },
    {
      avatar:
        "https://media.newyorker.com/photos/5ec2d7a40fe2fbfb61a298c8/4:3/w_1808,h_1356,c_limit/Russell-NormalPeople-3.jpg",
      isMentioned: true,
      UserName: "Rubaika",
      Activity: "mentioned you in a comment",
      Time: "9 hrs",
    },
    {
      avatar:
        "https://media.newyorker.com/photos/5ec2d7a40fe2fbfb61a298c8/4:3/w_1808,h_1356,c_limit/Russell-NormalPeople-3.jpg",
      isMentioned: false,
      UserName: "yungb00",
      Activity: "send you a message",
      Time: "23 hrs ago",
    },
    {
      avatar:
        "https://media.newyorker.com/photos/5ec2d7a40fe2fbfb61a298c8/4:3/w_1808,h_1356,c_limit/Russell-NormalPeople-3.jpg",
      isMentioned: false,
      UserName: "yungb00",
      Activity: "send you a message",
      Time: "23 hrs ago",
    },
    {
      avatar:
        "https://media.newyorker.com/photos/5ec2d7a40fe2fbfb61a298c8/4:3/w_1808,h_1356,c_limit/Russell-NormalPeople-3.jpg",
      isMentioned: false,
      UserName: "Everyday English-French-Spanish: Conversation and Fun - Joe!",
      Activity: "",
      Time: "2 days ago",
    },
  ]);
  const [ActiveMenuItem, setMenuItem] = useState(1);

  const OnSelectItem = (id) => {
    setMenuItem(id);
  };
  const OnDropDownMenu = () => {
    setdropdown(!dropdown);
  };
  return (
    <div>
      <Nav />
      <div
        style={{ paddingBottom: "100px" }}
        className={dropdown ? classes.dualcontainer : classes.container}
      >
        <div className={classes.Activities}>
          <div className={classes.mobileHeader}>
            <div className={`${classes.header} flex items-center`}>
              <Link to="/homepage">
                <img src="/img/arrow-left.png" alt="" />
              </Link>
              {dropdown ? null : (
                <Link to="/messages">
                  <img src="/img/messages.png" alt="" />
                </Link>
              )}
            </div>
            <div
              className={`${classes.Headerheading} flex items-center justify-center`}
            >
              <span>All Activity</span>&nbsp;
              <img
                src="/img/dropdown-down.png"
                alt=""
                onClick={OnDropDownMenu}
              />
            </div>
          </div>
          {activities.map((item, index) => {
            return (
              <Activities
                key={index}
                avatar={item.avatar}
                isMentioned={item.isMentioned}
                UserName={item.UserName}
                Activity={item.Activity}
                Time={item.Time}
                dropdown={dropdown}
              />
            );
          })}
        </div>
        <div className={dropdown ? classes.MenuEnable : classes.MenuDisable}>
          <div className={classes.MenuHandler}>
            <div className={classes.mobileHeader}>
              <div className={`${classes.header} flex items-center`}>
                <Link to="/notifications" onClick={OnDropDownMenu}>
                  <img src="/img/arrow-left.png" alt="" />
                </Link>
                <Link to="/messages">
                  <img src="/img/messages.png" alt="" />
                </Link>
              </div>
              <div
                className={`${classes.Headerheading} flex items-center justify-center`}
              >
                <span>All Activity</span>&nbsp;
                <img
                  src="/img/dropdown-up.png"
                  alt=""
                  onClick={OnDropDownMenu}
                />
              </div>
            </div>
            <div className={classes.MobileView}>
              <div className={classes.ChatHandler}>
                {activities.map((item, index) => {
                  return (
                    <Activities
                      key={index}
                      avatar={item.avatar}
                      isMentioned={item.isMentioned}
                      UserName={item.UserName}
                      Activity={item.Activity}
                      Time={item.Time}
                      dropdown={dropdown}
                    />
                  );
                })}
              </div>
              <div className={classes.Menus}>
                <div
                  className={classes.MenuItem}
                  onClick={() => OnSelectItem(1)}
                >
                  <img src="/img/Activity.png" alt="" />
                  <div>All Activity</div>
                  {ActiveMenuItem === 1 ? (
                    <img src="/img/tick2.png" alt="" />
                  ) : (
                    <img src="/img/bluestar.png" alt="" />
                  )}
                </div>
                <div
                  className={classes.MenuItem}
                  onClick={() => OnSelectItem(2)}
                >
                  <img src="/img/likes.png" alt="" />
                  <div>Likes</div>
                  {ActiveMenuItem === 2 ? (
                    <img src="/img/tick2.png" alt="" />
                  ) : (
                    <img src="/img/bluestar.png" alt="" />
                  )}
                </div>
                <div
                  className={classes.MenuItem}
                  onClick={() => OnSelectItem(3)}
                >
                  <img src="/img/comment.png" alt="" />
                  <div>Comment</div>
                  {ActiveMenuItem === 3 ? (
                    <img src="/img/tick2.png" alt="" />
                  ) : (
                    <img src="/img/bluestar.png" alt="" />
                  )}
                </div>
                <div
                  className={classes.MenuItem}
                  onClick={() => OnSelectItem(4)}
                >
                  <img src="/img/qa.png" alt="" />
                  <div>Q & A</div>
                  {ActiveMenuItem === 4 ? (
                    <img src="/img/tick2.png" alt="" />
                  ) : (
                    <img src="/img/bluestar.png" alt="" />
                  )}
                </div>
                <div
                  className={classes.MenuItem}
                  onClick={() => OnSelectItem(5)}
                >
                  <img src="/img/gift.png" alt="" />
                  <div>Gifts</div>
                  {ActiveMenuItem === 5 ? (
                    <img src="/img/tick2.png" alt="" />
                  ) : (
                    <img src="/img/bluestar.png" alt="" />
                  )}
                </div>
                <div
                  className={classes.MenuItem}
                  onClick={() => OnSelectItem(6)}
                >
                  <img src="/img/subscribers.png" alt="" />
                  <div>Subscribers</div>
                  {ActiveMenuItem === 6 ? (
                    <img src="/img/tick2.png" alt="" />
                  ) : (
                    <img src="/img/bluestar.png" alt="" />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer Msg={true} />
    </div>
  );
};
