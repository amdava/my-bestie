import React from "react";
import classes from "./Home2.module.css";
import { Heading } from "./components/Heading";
import { Button } from "antd";
import { ArrowRightOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
export const Home2 = () => {
  return (
    <div className={classes.HomeContainer}>
      <Heading heading="Start earning money now!" />
      <div className={classes.cardContainer}>
        <div className={classes.Card}>
          <div className={classes.imgContainer}>
            <span className={classes.Badge}>1</span>
            <div className={classes.img}>
              <img src="/img/1.png"  alt=""/>
            </div>
          </div>
          <div className={classes.Name}>Create a free account</div>
          <div className={classes.paragraph}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
            minim veniam.
          </div>
        </div>
        <div className={classes.Card}>
          <div className={classes.imgContainer}>
            <span className={classes.Badge}>2</span>
            <div className={classes.img}>
              <img src="/img/2.png" alt=""/>
            </div>
          </div>
          <div className={classes.Name}>Create great content</div>
          <div className={classes.paragraph}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
            minim veniam.
          </div>
        </div>
        <div className={classes.Card}>
          <div className={classes.imgContainer}>
          <span className={classes.Badge}>3</span>
            <div className={classes.img}>
              <img src="/img/3.png" alt=""/>
            </div>
          </div>
          <div className={classes.Name}>Grow your market</div>
          <div className={classes.paragraph}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
            minim veniam.
          </div>
        </div>
      </div>
      <Link to="/signup">
        <Button className={classes.LearnMore}>
          Learn more&nbsp;
          <ArrowRightOutlined />
        </Button>
      </Link>
    </div>
  );
};
