import React from "react";
import classes from "./Home3.module.css";
import { Heading } from "./components/Heading";
import { Button } from "antd"; 
import { Link } from "react-router-dom";
export const Home3 = () => {
  return (
    <div className={classes.HomeContainer}>
      <Heading heading="Explore Our Creators" />
      <div className={classes.cardContainer}>
        <div className={classes.Card}>
          <div className={classes.img}>
            <img src="/img/4.png"  width="100%" height="100%" alt=""/>
          </div>
          <div className={classes.Name}>Animation</div>
          <div className={classes.paragraph}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
            minim veniam.
          </div>
        </div>
        <div className={classes.Card}>
          <div className={classes.img}>
            <img src="/img/5.png" width="100%" height="100%" alt=""/>
          </div>
          <div className={classes.Name}>Artist</div>
          <div className={classes.paragraph}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
            minim veniam.
          </div>
        </div>
        <div className={classes.Card}>
          <div className={classes.img}>
            <img src="/img/6.png" width="100%" height="100%" alt=""/>
          </div>
          <div className={classes.Name}>Designer</div>
          <div className={classes.paragraph}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
            minim veniam.
          </div>
        </div>
        <div className={classes.Card}>
          <div className={classes.img}>
            <img src="/img/7.png" width="100%" height="100%" alt=""/>
          </div>
          <div className={classes.Name}>Developer</div>
          <div className={classes.paragraph}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
            minim veniam.
          </div>
        </div>
      </div>
      <Link to="/signup">
          <Button className={classes.LearnMore} size="large">
              EXPLORE
          </Button>
        </Link>
    </div>
  );
};
