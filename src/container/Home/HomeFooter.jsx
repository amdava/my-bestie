import classes from "./HomeFooter.module.css";
import React from "react";
import { Row, Col, Button } from "antd";
import { Link } from "react-router-dom";
import {
  FacebookFilled,
  TwitterSquareFilled,
  YoutubeFilled,
  InstagramFilled,
  GithubFilled,
} from "@ant-design/icons";
export const HomeFooter = () => {
  return (
    <>
      <div className={classes.HomeFooter}>
        <div className={classes.HomeFooterC1}>
          <img
            src="/img/logo.png"
            width="100%"
            alt=""
            className={classes.imgHandler}
          />
          <p className={classes.paragraph}>
            Keep connect with us! Follow us on any of these platforms
          </p>
          <Row gutter={[16, 16]}>
            <Col span={8}>
              <FacebookFilled className={classes.IconHandler} />
            </Col>
            <Col span={8}>
              <GithubFilled className={classes.IconHandler} />
            </Col>
            <Col span={8}>
              <YoutubeFilled className={classes.IconHandler} />
            </Col>
            <Col span={8}>
              <InstagramFilled className={classes.IconHandler} />
            </Col>
            <Col span={8}>
              <img src="/img/pinterest.png" className={classes.IconHandler} alt="" />
            </Col>
            <Col span={8}>
              <TwitterSquareFilled className={classes.IconHandler} />
            </Col>
          </Row>
        </div>
        <div className={classes.HomeFooterC2}>
          <h1>ABOUT</h1>
          <ul>
            <li>
              <Link to="/" className={classes.Link}>Term of Services</Link>
            </li>
            <li>
              <Link to="/" className={classes.Link}>Privacy</Link>
            </li>
            <li>
              <Link to="/" className={classes.Link}>About us</Link>
            </li>
            <li>
              <Link to="/" className={classes.Link}>How its works</Link>
            </li>
            <li>
              <Link to="/" className={classes.Link}>Cookies Policy</Link>
            </li>
            <li>
              <Link to="/" className={classes.Link}>Contact us</Link>
            </li>
            <li>
              <Link to="/" className={classes.Link}>Blog</Link>
            </li>
          </ul>
        </div>
        <div className={classes.HomeFooterC3}>
          <h1>CATOGORIES</h1>
          <ul>
            <li>
              <a href="#">Animation</a>
            </li>
            <li>
              <a href="#">Artist</a>
            </li>
            <li>
              <a href="#">Designer</a>
            </li>
            <li>
              <a href="#">Developer</a>
            </li>
            <li>
              <a href="#">Developer</a>
            </li>
            <li>
              <a href="#">Drawing & Painting</a>
            </li>
            <li>
              <a href="#">Contact us</a>
            </li>
            <li>
              <a href="#">Blog</a>
            </li>
          </ul>
        </div>
        <div className={classes.HomeFooterC4}>
          <h1>LINKS</h1>
          <ul>
            <li>
              <Link to="/login" className={classes.Link}>
                Login
              </Link>
            </li>
            <li>
              <Link to="/signup" className={classes.Link}>
                signup
              </Link>
            </li>
            <li>
              <Link to="/" className={classes.Link}>English</Link>
            </li>
          </ul>
        </div>
      </div>
      <div className={classes.HomeFooterC5}>
        <div style={{display:"flex"}}>
        <img src="/img/logo.png" width="100%" className={classes.imgHandler} alt=""/>
        <p className={classes.paragraph}>
          Keep connect with us! Follow us on any of these platforms
        </p>
        </div>
        <Row gutter={[16, 16]}>
          <Col span={8}>
            <FacebookFilled className={classes.IconHandler} />
          </Col>
          <Col span={8}>
            <GithubFilled className={classes.IconHandler} />
          </Col>
          <Col span={8}>
            <YoutubeFilled className={classes.IconHandler} />
          </Col>
          <Col span={8}>
            <InstagramFilled className={classes.IconHandler} />
          </Col>
          <Col span={8}>
            <img src="/img/pinterest.png" className={classes.IconHandler} alt=""/>
          </Col>
          <Col span={8}>
            <TwitterSquareFilled className={classes.IconHandler} />
          </Col>
        </Row>
      </div>

      <div className={classes.LastFooter}>
        <span style={{ padding: "20px 0" }}>
          This site uses cookies, by continuing to use the service, you accept
          our use of cookies{" "}
          <Link to="/" style={{ color: "#2BC2D3" }}>Cookies Policy</Link>&nbsp;&nbsp;
        </span>
        <Button
          type="primary"
          size="large"
          style={{ backgroundColor: "#51F0B0", color: "white", border: "none" }}
        >
          GOT IT
        </Button>
      </div>
    </>
  );
};
