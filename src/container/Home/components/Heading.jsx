import classes from "./Heading.module.css";
import React from "react";

export const Heading = ({ heading }) => {
  return <div className={classes.heading}>{heading}</div>;
};
