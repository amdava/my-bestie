import React, { useState } from "react";
import classes from "./signup.module.css";
import { Form, Input, Button, Checkbox, Select, Alert } from "antd";
import { UserOutlined, LockOutlined, MailOutlined } from "@ant-design/icons";
import { Link, useHistory } from "react-router-dom";

import { Nav } from "../../../component/nav/nav";
import { Header } from "../../../mobileReuseable/blueheader/header";
import { Footer } from "../../../component/Footer/Footer";
import { BackDrop } from "../../../component/backdrop/backdrop";
import IconCake from "./outline-cake-24px.png";
import IconBuilding from "./apartment-24px.png";
import { useAuth } from "../../../contexts/AuthContext";
import { store } from "../../../firebase";

const { Option } = Select;

export const SignupPage = () => {
  const { signup, OnTwitterLogin } = useAuth();
  const data = ["New York", "Alaska", "Vietnam"];
  const history = useHistory();
  const [error, seterror] = useState("");
  const [Loading, setLoading] = useState(false);
  const OnTwitterSignIn = () => {
    try {
      OnTwitterLogin();
    } catch (error) {
      console.log(error);
    }
  };
  const onFinish = async (values) => {
    console.log(values);
    try {
      seterror("");
      setLoading(true);
      await signup(values.email, values.password).then(async (result) => {
        console.log(result.user.uid);

        store
          .collection("users")
          .doc(result.user.uid)
          .set({
            userName: values.username,
            email: values.email,
            password: values.password,
            state: values.country,
            dob: values.dob,
            createdAt: new Date(),
            uid: result.user.uid,
            IsOnline: true,
            haveStatus:false,
            userType: "normal",
            photo: "",
            cover: "",
            coins: 0,
          })
          .then(async (docRef) => {
            await localStorage.setItem("isLoggedin", 1);
            // localStorage.setItem('userdata',JSON.stringify(values))
            await setLoading(false);
            history.push("/homepage");
          });
      });
    } catch (error) {
      seterror("Failed to create an account");
      setLoading(false);
    }
  };

  const signupdata = (
    <>
      <div className={classes.formcontainer}>
        <div className={classes.mobileHeading}>
          <span>
            <Link to="/login" style={{ color: "white" }}>
              LOG IN
            </Link>
          </span>
          <span className={classes.signinselected}>
            <Link to="/signup" style={{ color: "white" }}>
              SIGN UP
            </Link>
          </span>
        </div>
        {error ? <Alert message={error} type="error" /> : null}
        <Form
          name="signup_login"
          className={classes.formCss}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Please input your Username!",
              },
            ]}
          >
            <Input
              style={{ border: "none", borderBottom: "2px solid #DDDDDD" }}
              prefix={<UserOutlined className={classes.IconStyle} />}
              placeholder="&nbsp;&nbsp;Name"
            />
          </Form.Item>
          <Form.Item
            name="email"
            hasFeedback
            rules={[
              {
                type: "email",
                required: true,
                validateStatus: "success",
                message: "Please input your email!",
              },
            ]}
          >
            <Input
              style={{ border: "none", borderBottom: "2px solid #DDDDDD" }}
              prefix={<MailOutlined className={classes.IconStyle} />}
              placeholder="&nbsp;&nbsp;Email"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your Password!",
              },
            ]}
          >
            <Input.Password
              style={{ border: "none", borderBottom: "2px solid #DDDDDD" }}
              prefix={<LockOutlined className={classes.IconStyle} />}
              type="password"
              placeholder="&nbsp;&nbsp;Password"
            />
          </Form.Item>
          <Form.Item
            name="confirm password"
            rules={[
              {
                required: true,
                message: "Please confirm your Password!",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error("The two passwords doesn't match!")
                  );
                },
              }),
            ]}
          >
            <Input.Password
              style={{ border: "none", borderBottom: "2px solid #DDDDDD" }}
              prefix={<LockOutlined className={classes.IconStyle} />}
              type="password"
              placeholder="&nbsp;&nbsp;Confirm Password"
            />
          </Form.Item>
          <Form.Item
            name="dob"
            rules={[
              {
                required: true,
                message: "Please input your date of birth!",
              },
            ]}
          >
            <Input
              style={{ border: "none", borderBottom: "2px solid #DDDDDD" }}
              prefix={<img src={IconCake} alt="" />}
              placeholder="&nbsp;Date of Birth"
            />
          </Form.Item>
          <Form.Item
            name="country"
            rules={[
              {
                required: true,
                message: "Please select Country or State!",
              },
            ]}
          >
            <Select
              style={{
                border: "none",
                borderBottom: "2px solid #DDDDDD",
                textAlign: "left",
              }}
              bordered={false}
              allowClear
              // suffixIcon={<UserOutlined className={classes.IconStyle} />}
              optionLabelProp="label"
              placeholder={
                <>
                  <img src={IconBuilding} alt="" />
                  &nbsp;&nbsp;Country or State
                </>
              }
            >
              {data.map((item, index) => {
                return (
                  <Option
                    key={index}
                    value={item}
                    label={
                      <div className="flex items-center">
                        <img src={IconBuilding} alt="" className="h-5"/>&nbsp;
                        {item}
                      </div>
                    }
                  >
                    {item}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item>
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <Checkbox className={classes.RememberMe}>Remember me</Checkbox>
            </Form.Item>
          </Form.Item>
          <Form.Item>
            <Button
              size="medium"
              className={classes.Buttons}
              type="primary"
              htmlType="submit"
            >
              Register
            </Button>
          </Form.Item>
          <div className={classes.AccountInfo}>
            Already have an account? <Link to="/login">Log In</Link>
          </div>
        </Form>
        &nbsp;
        <div style={{ fontWeight: "bold", color: "#2BC2D3" }}>OR</div>
        &nbsp;
        <Button
          size="medium"
          className={classes.ButtonsTwitter}
          type="primary"
          onClick={OnTwitterSignIn}
          htmlType="submit"
          disabled={Loading}
        >
          <img src="/img/twitter-white.png" alt="" />
          &nbsp;&nbsp; SIGN IN WITH TWITTER
        </Button>
      </div>
      <div className={classes.heading}>SIGN UP</div>
    </>
  );

  return (
    <>
      <BackDrop loading={Loading} />
      <div className={classes.desktopDesign}>
        <Nav />
        <div className={classes.container}>{signupdata}</div>
        <Footer />
      </div>
      <div className={classes.mobileDesign}>
        <Header forwardTo="/signup" backTo="/signup" />
        {signupdata}
        <div className={classes.MobileFooter}>
          By logging up, you are agree with our{" "}
          <span style={{ color: "#2BC2D3", borderBottom: "#2BC2D3" }}>
            Terms & Conditions
          </span>
        </div>
      </div>
    </>
  );
};
