import React, { useState, useEffect } from "react";
import { Row, Col, Pagination } from "antd";
import classes from "./suggestions.module.css";
import { CardFun } from "./suggestCard/card";
import { RightOutlined, LeftOutlined, RedoOutlined, TagOutlined } from "@ant-design/icons";
import { isEmpty } from "../../../../utils/functions/isObjectEmpty";
import { store } from "../../../../firebase";
export const Suggestions = () => {
	// const { data } = useSelector((state) => state.UserReducer);
	const [usersForSuggestions, setusersForSuggestions] = useState([]);
	const [suggestionsPerPage] = useState(3);
	const [currentSuggestionPage, setcurrentSuggestionPage] = useState(1);

	const [startpoint, setstartpoint] = useState();

	useEffect(() => {
		const Arr = [];
		store
			.collection("users")
			.limit(3)
			.get()
			.then(async (snapshot) => {
				var lastvisible = snapshot.docs[snapshot.docs.length - 1];
				setstartpoint(lastvisible); //for next page
				await snapshot.forEach((doc) => {
					Arr.push(doc.data());
				});
				setusersForSuggestions(Arr);
			});
	}, []);

	const indexOfLastPost = currentSuggestionPage * suggestionsPerPage;
	const indexOfFirstPost = indexOfLastPost - suggestionsPerPage;

	const Prevpaginate = () => {
		currentSuggestionPage > 1 && setcurrentSuggestionPage(currentSuggestionPage - 1);
	};

	const Nextpaginate = async () => {
		console.log(indexOfLastPost, usersForSuggestions.length);
		if (indexOfLastPost <= usersForSuggestions.length) {
			await store
				.collection("users")
				.startAfter(startpoint)
				.limit(3)
				.get()
				.then(async (snapshot) => {
					var lastvisible = snapshot.docs[snapshot.docs.length - 1];
					if (snapshot.docs.length !== 0) {
						setstartpoint(lastvisible); //for next page
						await snapshot.forEach((doc) => {
							usersForSuggestions.push(doc.data());
						});
						setusersForSuggestions(usersForSuggestions);
					}
				});
			setcurrentSuggestionPage(currentSuggestionPage + 1);
		}
	};

	// console.log(usersForSuggestions);
	const ResetSuggestions = () => {
		if (currentSuggestionPage > 1) {
			setcurrentSuggestionPage(1);
			// const Arr = [];
			// store
			// 	.collection("users")
			// 	.limit(3)
			// 	.get()
			// 	.then(async (snapshot) => {
			// 		var lastvisible = snapshot.docs[snapshot.docs.length - 1];
			// 		setstartpoint(lastvisible); //for next page
			// 		await snapshot.forEach((doc) => {
			// 			Arr.push(doc.data());
			// 		});
			// 		await setusersForSuggestions(Arr);

			// 	});
		}
	};

	return (
		<div className={classes.sugestedScreen}>
			<Row justify="space-between" className="items-center">
				<Col className={classes.headingStyle}>Suggestions</Col>
				<Col>
					<TagOutlined />
				</Col>
				<Col onClick={ResetSuggestions}>
					<RedoOutlined
						style={currentSuggestionPage > 1 ? { color: "black" } : { color: "gray" }}
					/>
				</Col>
				<Col onClick={Prevpaginate}>
					<LeftOutlined
						className="cursor-pointer"
						style={currentSuggestionPage === 1 ? { color: "rgba(0,0,0,0.2)" } : { color: "black" }}
					/>
				</Col>
				<Col onClick={Nextpaginate}>
					<RightOutlined
						className="cursor-pointer"
						style={
							indexOfLastPost > usersForSuggestions.length
								? { color: "rgba(0,0,0,0.2)" }
								: { color: "black" }
						}
					/>
				</Col>
			</Row>

			{usersForSuggestions.slice(indexOfFirstPost, indexOfLastPost).map((item, index) => {
				return <CardFun key={index} data={item} />;
			})}
		</div>
	);
};
