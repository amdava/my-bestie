import classes from "./card.module.css";
import React from "react";
import { Card, Avatar, Image } from "antd";
// import classes from "*.module.css";

const { Meta } = Card;
export const CardFun = ({ data }) => {
	const {
		IsOnline,
		coins,
		cover,
		createdAt,
		dob,
		email,
		state,
		uid,
		userName,
		userType,
		photo,
	} = data;

	return (
		<Meta
			className={classes.CardData}
			avatar={
				<Avatar
					src={
						photo ? (
							photo
						) : (
							<div className="md:text-3xl lg:text-6xl">
								<span>{userName.charAt(0)}</span>
							</div>
						)
					}
					className={classes.avatar}
					alt=""
				/>
			}
			// title={}
			description={
				<div className={classes.bghandler}>
					<div className={`${classes.Cardtext} flex items-center`}>
						<span>{userName.substring(0, 10)}</span>&nbsp;
						{userType ? <img className={classes.tickIcon} src="/img/tick.png" alt="" /> : null}
					</div>
					<div className={classes.CardMail}>@{userName.split(" ")}</div>
					<div className={classes.CardSetting}>
						<img src="/img/settings.png" alt="" />
					</div>
					<Image
						src="https://cdn.pixabay.com/photo/2014/02/27/16/10/tree-276014__340.jpg"
						alt=""
						preview={false}
						// width="100%"
					/>
				</div>
			}
		/>
	);
};
