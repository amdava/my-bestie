import React, { useState, useEffect } from "react";
import { Post } from "./components/posts/post";
import { Status } from "./components/status/status";
import { Suggestions } from "./components/suggestions/suggestions";
import { Row, Col } from "antd";
import { Nav } from "../../component/nav/pages/nav";
import { Footer } from "../../component/Footer/Footer";
import classes from "./HomePage.module.css";
import { Header } from "../../mobileReuseable/blueheader/header";
import { PostMsgModal } from "../../mobileReuseable/modal/modal";
import { store } from "../../firebase";
import { getSelectedUser } from "../../redux/actions/user";
import { useDispatch } from "react-redux";

export const HomePage = () => {
	const dispatch = useDispatch();
	const [modalvisible, setmodalvisible] = useState(false);
	const [posts, setposts] = useState([]);
	useEffect(() => {
		store.collection("posts").onSnapshot((queryshot) => {
			const Arr = [];
			queryshot.forEach((doc) => {
				Arr.push(doc.data());
				// setposts([...posts, doc.data()]);
			});
			setposts(Arr);
		});
	}, []);

	const modalState = async (id) => {
		dispatch(getSelectedUser(id));
		modaltoggle();
	};

	const modaltoggle = () => {
		setmodalvisible(!modalvisible);
	}; 
	return (
		<>
			<div className={classes.desktopDesign}>
				<Nav />
				<Row
					justify="space-around"
					style={{ overflow: "hidden" }}
					className="min-h-screen bg-gray-200  lg:px-20"
				>
					<Col
						xs={{ span: 24 }}
						sm={{ span: 16 }}
						md={{ span: 16 }}
						lg={{ span: 14 }}
						className={classes.modalwidth}
					>
						<Status />
						<PostMsgModal visible={modalvisible} modal={modaltoggle} />
						{posts.length > 0 &&
							posts.map((item, index) => {
								return <Post key={index} userData={item} modal={modalState} />;
							})}
					</Col>
					<Col xs={{ span: 24 }} sm={{ span: 8 }} md={{ span: 8 }} lg={{ span: 8 }}>
						<Suggestions />
					</Col>
				</Row>
				<Footer Home={true} Explore={false} NewPost={false} Msg={false} Profile={false} />
			</div>
			<div className={classes.mobileDesign}>
				<div style={{ backgroundColor: "#8df5cb" }}>
					<Header
						heading="Feeds"
						backTo="/"
						forwardTo="/explore"
						imgLeft="/img/arrow-left.png"
						imgRight="/img/search.png"
					/>
				</div>
				<Row style={{ overflow: "hidden" }}>
					<Col xs={{ span: 24 }} sm={{ span: 16 }} md={{ span: 16 }} lg={{ span: 16 }}>
						<Status />
						{posts.map((item, index) => {
							return <Post key={index} userData={item} />;
						})}
					</Col>
					<Col xs={{ span: 24 }} sm={{ span: 8 }} md={{ span: 8 }} lg={{ span: 8 }}>
						<Suggestions />
					</Col>
				</Row>
				<Footer Home={true} Explore={false} NewPost={false} Msg={false} Profile={false} />
			</div>
		</>
	);
};
